# tempo-components-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your Documentation
```
npm run storybook
```

### Lints and fixes files
```
npm run lint
```

### Publish
```
change verions in package.json and package-lock.json
npm run build:npm
npm publish
```

### Deployment
```
Pipelines run in auto mode after any commit in branchs that contain:
**develop
**staging
**release
**bugfix
**master

But, in master and staging case, is necessary that the developer access the jenkins, and click in the approve button.

https://jenkins.tools.tempoassist.cloud/view/MFE/job/mfe-tempo-components-vue

The Developer can see the deployment version in Argo and AWS:
https://argocd.tools.tempoassist.cloud/applications/mfe-all-dev

https://us-east-1.console.aws.amazon.com/codesuite/codeartifact/d/257578036299/tempoassist/r/tempoassist/p/npm/tempoassist/tempo-components-vue/versions?region=us-east-1&package-versions-meta=eyJmIjp7fSwicyI6e30sIm4iOjIwLCJpIjowfQ

The developer can to, make a refresh and sync in argo, for remove the cache.
```

### Enjoy the Lib <3>

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

