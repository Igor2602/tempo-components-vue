import { withVuetify } from '@socheatsok78/storybook-addon-vuetify/dist/decorators';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

import Vuetify from 'vuetify'
// import colors from 'vuetify/es5/util/colors'
import 'vuetify/dist/vuetify.min.css'; // all the css for components

const vuetifyConfig = new Vuetify({
  theme: {
    light: true,
    dark: false,
    themes: {
      light: {
        background: '#F2F2F3',
        black: '#1E1C1F',
        red: '#DB4F4F',
        primary: '#00527D',
        gray: '#5F5C60',
        green: '#35A97F',
        blue: '#0096D3',
        gray2: '#748C94',
        dark: '#1A1E2E',
        lightBlue: '#5EBED4',
        lightGray: '#D9D8D9',
        darkBlue: '#2A2F41',
        accent: '#82B1FF',
        error: '#FF5858',
        info: '#F2F2F3',
        success: '#35A97F',
        warning: '#FFC107',
        white: '#ffffff',
      },
      dark: {
        primary: '#0096D3',
        blue: '#5EBED4',
        lightGray: '#748C94',
        background: '#1A1E2E',
        black: '#1E1C1F',
        red: '#DB4F4F',
        gray: '#5F5C60',
        green: '#35A97F',
        gray2: '#5EBED4', // lightBlue
        dark: '#1A1E2E',
        lightBlue: '#5EBED4',
        darkBlue: '#2A2F41',
      },
    },
  },
})

export const decorators = [(_) => {
  return {
    vuetify: vuetifyConfig,
    template: `
      <v-app>
        <v-main>
          <v-container fluid>
            <story />
          </v-container>
        </v-main>
      </v-app>`
  }
}];