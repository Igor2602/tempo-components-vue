import { create } from '@storybook/theming';

export default create({
  base: 'light',
  brandTitle: 'Tempo Assist Vue Components',
  brandUrl: 'https://bitbucket.org/Tempo_Assist/tempo-components-vue',
  brandImage: 'https://www.tempoassist.com.br/resources/logo/logo_tempo_3.png',
});