import TaAccordion from './components/taAccordion/TaAccordion.vue';
import TaAddPrimaryLabeled from './components/taButtons/taBtnAdd/taBtnAddLabeled/TaAddPrimaryLabeled.vue';
import TaAddSuccessLabeled from './components/taButtons/taBtnAdd/taBtnAddLabeled/TaAddSuccessLabeled.vue';
import TaAlertPrimaryActionError from './components/taAlert/action/primary/TaAlertPrimaryActionError.vue';
import TaAlertPrimaryActionInfo from './components/taAlert/action/primary/TaAlertPrimaryActionInfo.vue';
import TaAlertPrimaryActionSuccess from './components/taAlert/action/primary/TaAlertPrimaryActionSuccess.vue';
import TaAlertPrimaryError from './components/taAlert/alert/primary/TaAlertPrimaryError.vue';
import TaAlertPrimaryInfo from './components/taAlert/alert/primary/TaAlertPrimaryInfo.vue';
import TaAlertPrimarySuccess from './components/taAlert/alert/primary/TaAlertPrimarySuccess.vue';
import TaAlertSecondaryActionError from './components/taAlert/action/secondary/TaAlertSecondaryActionError.vue';
import TaAlertSecondaryActionInfo from './components/taAlert/action/secondary/TaAlertSecondaryActionInfo.vue';
import TaAlertSecondaryActionSuccess from './components/taAlert/action/secondary/TaAlertSecondaryActionSuccess.vue';
import TaAlertSecondaryError from './components/taAlert/alert/secondary/TaAlertSecondaryError.vue';
import TaAlertSecondaryInfo from './components/taAlert/alert/secondary/TaAlertSecondaryInfo.vue';
import TaAlertSecondarySuccess from './components/taAlert/alert/secondary/TaAlertSecondarySuccess.vue';
import TaAvaliation from './components/taAvaliation/TaAvaliation.vue';
import TaAvatar from './components/taAvatar/TaAvatar.vue';
import Avatar from './components/taAvatar/Avatar.vue';
import TaAvatarDetail from './components/taAvatar/TaAvatarDetail.vue';
import TaBreadcrumb from './components/taBreadcrumb/TaBreadcrumb.vue';
import TaBtnAddOutlined from './components/taButtons/taBtnAdd/tabtnAddOutlined/TaBtnAddOutlined.vue';
import TaBtnAddOutlinedLarge from './components/taButtons/taBtnAdd/tabtnAddOutlined/TaBtnAddOutlinedLarge.vue';
import TaBtnAddOutlinedSmall from './components/taButtons/taBtnAdd/tabtnAddOutlined/TaBtnAddOutlinedSmall.vue';
import TaBtnAddPrimary from './components/taButtons/taBtnAdd/taBtnAddPrimary/TaBtnAddPrimary.vue';
import TaBtnAddPrimaryLarge from './components/taButtons/taBtnAdd/taBtnAddPrimary/TaBtnAddPrimaryLarge.vue';
import TaBtnAddPrimarySmall from './components/taButtons/taBtnAdd/taBtnAddPrimary/TaBtnAddPrimarySmall.vue';
import TaBtnAddSecondary from './components/taButtons/taBtnAdd/taBtnAddSecondary/TaBtnAddSecondary.vue';
import TaBtnAddSecondaryLarge from './components/taButtons/taBtnAdd/taBtnAddSecondary/TaBtnAddSecondaryLarge.vue';
import TaBtnAddSecondarySmall from './components/taButtons/taBtnAdd/taBtnAddSecondary/TaBtnAddSecondarySmall.vue';
import TaBtnBackButton from './components/taButtons/taBtnBackButton/TaBtnBackButton.vue';
import TaBtnDownloadIconDelete from './components/taButtons/taBtnDownloadIcon/TaBtnDownloadIconDelete.vue';
import TaBtnDownloadIconOutlined from './components/taButtons/taBtnDownloadIcon/TaBtnDownloadIconOutlined.vue';
import TaBtnDownloadIconPrimary from './components/taButtons/taBtnDownloadIcon/TaBtnDownloadIconPrimary.vue';
import TaBtnDownloadIconSecondary from './components/taButtons/taBtnDownloadIcon/TaBtnDownloadIconSecondary.vue';
import TaBtnDownloadOutlined from './components/taButtons/taBtnDownload/tabtnDownloadOutlined/TaBtnDownloadOutlined.vue';
import TaBtnDownloadOutlinedLarge from './components/taButtons/taBtnDownload/tabtnDownloadOutlined/TaBtnDownloadOutlinedLarge.vue';
import TaBtnDownloadOutlinedSmall from './components/taButtons/taBtnDownload/tabtnDownloadOutlined/TaBtnDownloadOutlinedSmall.vue';
import TaBtnDownloadPrimary from './components/taButtons/taBtnDownload/taBtnDownloadPrimary/TaBtnDownloadPrimary.vue';
import TaBtnDownloadPrimaryLarge from './components/taButtons/taBtnDownload/taBtnDownloadPrimary/TaBtnDownloadPrimaryLarge.vue';
import TaBtnDownloadPrimarySmall from './components/taButtons/taBtnDownload/taBtnDownloadPrimary/TaBtnDownloadPrimarySmall.vue';
import TaBtnDownloadSecondary from './components/taButtons/taBtnDownload/taBtnDownloadSecondary/TaBtnDownloadSecondary.vue';
import TaBtnDownloadSecondaryLarge from './components/taButtons/taBtnDownload/taBtnDownloadSecondary/TaBtnDownloadSecondaryLarge.vue';
import TaBtnDownloadSecondarySmall from './components/taButtons/taBtnDownload/taBtnDownloadSecondary/TaBtnDownloadSecondarySmall.vue';
import TaBtnDropDownOutlined from './components/taButtons/taBtnDropDown/TaBtnDropDownOutlined.vue';
import TaBtnDropDownPrimary from './components/taButtons/taBtnDropDown/TaBtnDropDownPrimary.vue';
import TaBtnFabLabeledPrimary from './components/taButtons/taBtnFab/labeled/TaBtnFabLabeledPrimary.vue';
import TaBtnFabLabeledSecondary from './components/taButtons/taBtnFab/labeled/TaBtnFabLabeledSecondary.vue';
import TaBtnFabPrimary from './components/taButtons/taBtnFab/icon/TaBtnFabPrimary.vue';
import TaBtnFabSecondary from './components/taButtons/taBtnFab/icon/TaBtnFabSecondary.vue';
import TaBtnGroupOutlined from './components/taButtons/taBtnGroup/TaBtnGroupOutlined.vue';
import TaBtnGroupPrimary from './components/taButtons/taBtnGroup/TaBtnGroupPrimary.vue';
import TaBtnGroupRounded from './components/taButtons/taBtnGroup/TaBtnGroupRounded.vue';
import TaBtnNextOutlined from './components/taButtons/taBtnNext/tabtnNextOutlined/TaBtnNextOutlined.vue';
import TaBtnNextOutlinedLarge from './components/taButtons/taBtnNext/tabtnNextOutlined/TaBtnNextOutlinedLarge.vue';
import TaBtnNextOutlinedSmall from './components/taButtons/taBtnNext/tabtnNextOutlined/TaBtnNextOutlinedSmall.vue';
import TaBtnNextPrimary from './components/taButtons/taBtnNext/taBtnNextPrimary/TaBtnNextPrimary.vue';
import TaBtnNextPrimaryLarge from './components/taButtons/taBtnNext/taBtnNextPrimary/TaBtnNextPrimaryLarge.vue';
import TaBtnNextPrimarySmall from './components/taButtons/taBtnNext/taBtnNextPrimary/TaBtnNextPrimarySmall.vue';
import TaBtnNextSecondary from './components/taButtons/taBtnNext/taBtnNextSecondary/TaBtnNextSecondary.vue';
import TaBtnNextSecondaryLarge from './components/taButtons/taBtnNext/taBtnNextSecondary/TaBtnNextSecondaryLarge.vue';
import TaBtnNextSecondarySmall from './components/taButtons/taBtnNext/taBtnNextSecondary/TaBtnNextSecondarySmall.vue';
import TaBtnRefreshOutlined from './components/taButtons/taBtnRefresh/taBtnRefreshOutlined/TaBtnRefreshOutlined.vue';
import TaBtnRefreshOutlinedLarge from './components/taButtons/taBtnRefresh/taBtnRefreshOutlined/TaBtnRefreshOutlinedLarge.vue';
import TaBtnRefreshOutlinedSmall from './components/taButtons/taBtnRefresh/taBtnRefreshOutlined/TaBtnRefreshOutlinedSmall.vue';
import TaBtnRefreshPrimary from './components/taButtons/taBtnRefresh/taBtnRefreshPrimary/TaBtnRefreshPrimary.vue';
import TaBtnRefreshPrimaryLarge from './components/taButtons/taBtnRefresh/taBtnRefreshPrimary/TaBtnRefreshPrimaryLarge.vue';
import TaBtnRefreshPrimarySmall from './components/taButtons/taBtnRefresh/taBtnRefreshPrimary/TaBtnRefreshPrimarySmall.vue';
import TaBtnRefreshSecondary from './components/taButtons/taBtnRefresh/taBtnRefreshSecondary/TaBtnRefreshSecondary.vue';
import TaBtnRefreshSecondaryLarge from './components/taButtons/taBtnRefresh/taBtnRefreshSecondary/TaBtnRefreshSecondaryLarge.vue';
import TaBtnRefreshSecondarySmall from './components/taButtons/taBtnRefresh/taBtnRefreshSecondary/TaBtnRefreshSecondarySmall.vue';
import TaBtnOutlined from './components/taButtons/taBtn/tabtnOutlined/TaBtnOutlined.vue';
import TaBtnOutlinedLarge from './components/taButtons/taBtn/tabtnOutlined/TaBtnOutlinedLarge.vue';
import TaBtnOutlinedSmall from './components/taButtons/taBtn/tabtnOutlined/TaBtnOutlinedSmall.vue';
import TaBtnPrimary from './components/taButtons/taBtn/taBtnPrimary/TaBtnPrimary.vue';
import TaBtnPrimaryLarge from './components/taButtons/taBtn/taBtnPrimary/TaBtnPrimaryLarge.vue';
import TaBtnPrimarySmall from './components/taButtons/taBtn/taBtnPrimary/TaBtnPrimarySmall.vue';
import TaBtnSecondary from './components/taButtons/taBtn/taBtnSecondary/TaBtnSecondary.vue';
import TaBtnSecondaryLarge from './components/taButtons/taBtn/taBtnSecondary/TaBtnSecondaryLarge.vue';
import TaBtnSecondarySmall from './components/taButtons/taBtn/taBtnSecondary/TaBtnSecondarySmall.vue';
import TaBtnUploadIconDelete from './components/taButtons/taBtnUploadIcon/TaBtnUploadIconDelete.vue';
import TaBtnUploadIconOutlined from './components/taButtons/taBtnUploadIcon/TaBtnUploadIconOutlined.vue';
import TaBtnUploadIconPrimary from './components/taButtons/taBtnUploadIcon/TaBtnUploadIconPrimary.vue';
import TaBtnUploadIconSecondary from './components/taButtons/taBtnUploadIcon/TaBtnUploadIconSecondary.vue';
import TaCard from './components/taCards/taCard/TaCard.vue';
import TaCardDetail from './components/taCards/taCard/TaCardDetail.vue';
import TaCardDetailSeal from './components/taCards/taCard/TaCardDetailSeal.vue';
import TaCardDetailSecundary from './components/taCards/taCard/TaCardDetailSecundary.vue';
import TaCardInfo from './components/taCards/taCard/TaCardInfo.vue';
import TaCardOption from './components/taCards/taCardOption/TaCardOption.vue';
import TaCardOptionGroup from './components/taCards/taCardOption/TaCardOptionGroup.vue';
import TaCheckboxPrimary from './components/taCheckbox/TaCheckboxPrimary.vue';
import TaCheckboxSecundary from './components/taCheckbox/TaCheckboxSecundary.vue';
import TaChip from './components/taChip/chip/TaChip.vue';
import TaChipOutlined from './components/taChip/chip/TaChipOutlined.vue';
import TaChipPrimary from './components/taChip/primary/TaChipPrimary.vue';
import TaChipPrimaryOutlined from './components/taChip/primary/TaChipPrimaryOutlined.vue';
import TaChipSecondary from './components/taChip/secondary/TaChipSecondary.vue';
import TaChipSecondaryOutlined from './components/taChip/secondary/TaChipSecondaryOutlined.vue';
import TaCustomDataTable from './components/taTables/taTable/TaCustomDataTable.vue';
import TaDatepicker from './components/taDatepicker/TaDatepicker.vue';
import TaDialogConfirmation from './components/taDialog/TaDialogConfirmation.vue';
import TaDialogList from './components/taDialog/TaDialogList.vue';
import TaDrawer from './components/taDrawer/TaDrawer.vue';
import TaListInfo from './components/taListInfo/TaListInfo.vue';
import TaLoaderPrimary from './components/taLoader/primary/TaLoaderPrimary.vue';
import TaLoaderPrimaryLarge from './components/taLoader/primary/TaLoaderPrimaryLarge.vue';
import TaLoaderPrimarySmall from './components/taLoader/primary/TaLoaderPrimarySmall.vue';
import TaLoaderSecondary from './components/taLoader/secondary/TaLoaderSecondary.vue';
import TaLoaderSecondaryLarge from './components/taLoader/secondary/TaLoaderSecondaryLarge.vue';
import TaLoaderSecondarySmall from './components/taLoader/secondary/TaLoaderSecondarySmall.vue';
import TaLocation from './components/taLocation/TaLocation.vue';
import TaPaginationPrimary from './components/taTables/taPagination/TaPaginationPrimary.vue';
import TaPaginationSecundary from './components/taTables/taPagination/TaPaginationSecundary.vue';
import TaProgressBar from './components/taBar/TaProgressBar.vue';
import TaRadioButton from './components/taRadioButton/TaRadioButton.vue';
import TaSnackbar from './components/taSnackbar/TaSnackbar.vue';
import TaSealError from './components/taSeal/TaSealError.vue';
import TaSealInfo from './components/taSeal/TaSealInfo.vue';
import TaSealPrimary from './components/taSeal/TaSealPrimary.vue';
import TaSealSuccess from './components/taSeal/TaSealSuccess.vue';
import TaSealUnavailable from './components/taSeal/TaSealUnavailable.vue';
import TaSealWarning from './components/taSeal/TaSealWarning.vue';
import TaSelect from './components/taSelect/TaSelect.vue';
import TaSelectMultiple from './components/taSelect/TaSelectMultiple.vue';
import TaSlider from './components/taSliders/TaSlider.vue';
import TaSliderRange from './components/taSliders/TaSliderRange.vue';
import TaSteps from './components/taSteps/TaSteps.vue';
import TaSwitch from './components/taSwitchs/taSwitch/TaSwitch.vue';
import TaSwitchBar from './components/taBar/TaSwitchBar.vue';
import TaTabBar from './components/taBar/TaTabBar.vue';
import TaAppToolbar from './components/taBar/TaAppToolbar.vue';
import TaTabsPrimary from './components/taTabs/TaTabsPrimary.vue';
import TaTabsSecundary from './components/taTabs/TaTabsSecundary.vue';
import TaTextField from './components/taTextField/TaTextField.vue';
import TaTextFieldCompanyDoc from './components/taTextField/TaTextFieldCompanyDoc.vue';
import TatextFieldCurrency from './components/taTextField/TatextFieldCurrency.vue';
import TaTextFieldPersonalDoc from './components/taTextField/TaTextFieldPersonalDoc.vue';
import TaTextFieldZipCode from './components/taTextField/TaTextFieldZipCode.vue';
import TaToolBar from './components/taBar/TaToolBar.vue';
import TaValueControl from './components/taValueControl/TaValueControl.vue';
import TaValueControlOutlined from './components/taValueControl/TaValueControlOutlined.vue';

// Declare install function executed by Vue.use()
export function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Vue.component('taAccordion, TaAccordion');
  Vue.component('taAddPrimaryLabeled, TaAddPrimaryLabeled');
  Vue.component('taAddSuccessLabeled, TaAddSuccessLabeled');
  Vue.component('taAlertPrimaryActionError, TaAlertPrimaryActionError');
  Vue.component('taAlertPrimaryActionInfo, TaAlertPrimaryActionInfo');
  Vue.component('taAlertPrimaryActionSuccess, TaAlertPrimaryActionSuccess');
  Vue.component('taAlertPrimaryError, TaAlertPrimaryError');
  Vue.component('taAlertPrimaryInfo, TaAlertPrimaryInfo');
  Vue.component('taAlertPrimarySuccess, TaAlertPrimarySuccess');
  Vue.component('taAlertSecondaryActionError, TaAlertSecondaryActionError');
  Vue.component('taAlertSecondaryActionInfo, TaAlertSecondaryActionInfo');
  Vue.component('taAlertSecondaryActionSuccess, TaAlertSecondaryActionSuccess');
  Vue.component('taAlertSecondaryError, TaAlertSecondaryError');
  Vue.component('taAlertSecondaryInfo, TaAlertSecondaryInfo');
  Vue.component('taAlertSecondarySuccess, TaAlertSecondarySuccess');
  Vue.component('taAvaliation, TaAvaliation');
  Vue.component('avatar, Avatar');
  Vue.component('taAvatar, TaAvatar');
  Vue.component('taAvatarDetail, TaAvatarDetail');
  Vue.component('taBreadcrumb, TaBreadcrumb');
  Vue.component('taBtnAddOutlined, TaBtnAddOutlined');
  Vue.component('taBtnAddOutlinedLarge, TaBtnAddOutlinedLarge');
  Vue.component('taBtnAddOutlinedSmall, TaBtnAddOutlinedSmall');
  Vue.component('taBtnAddPrimary, TaBtnAddPrimary');
  Vue.component('taBtnAddPrimaryLarge, TaBtnAddPrimaryLarge');
  Vue.component('taBtnAddPrimarySmall, TaBtnAddPrimarySmall');
  Vue.component('taBtnAddSecondary, TaBtnAddSecondary');
  Vue.component('taBtnAddSecondaryLarge, TaBtnAddSecondaryLarge');
  Vue.component('taBtnAddSecondarySmall, TaBtnAddSecondarySmall');
  Vue.component('taBtnBackButton, TaBtnBackButton');
  Vue.component('taBtnDownloadIconDelete, TaBtnDownloadIconDelete');
  Vue.component('taBtnDownloadIconOutlined, TaBtnDownloadIconOutlined');
  Vue.component('taBtnDownloadIconPrimary, TaBtnDownloadIconPrimary');
  Vue.component('taBtnDownloadIconSecondary, TaBtnDownloadIconSecondary');
  Vue.component('taBtnDownloadOutlined, TaBtnDownloadOutlined');
  Vue.component('taBtnDownloadOutlinedLarge, TaBtnDownloadOutlinedLarge');
  Vue.component('taBtnDownloadOutlinedSmall, TaBtnDownloadOutlinedSmall');
  Vue.component('taBtnDownloadPrimary, TaBtnDownloadPrimary');
  Vue.component('taBtnDownloadPrimaryLarge, TaBtnDownloadPrimaryLarge');
  Vue.component('taBtnDownloadPrimarySmall, TaBtnDownloadPrimarySmall');
  Vue.component('taBtnDownloadSecondary, TaBtnDownloadSecondary');
  Vue.component('taBtnDownloadSecondaryLarge, TaBtnDownloadSecondaryLarge');
  Vue.component('taBtnDownloadSecondarySmall, TaBtnDownloadSecondarySmall');
  Vue.component('taBtnDropDownOutlined, TaBtnDropDownOutlined');
  Vue.component('taBtnDropDownPrimary, TaBtnDropDownPrimary');
  Vue.component('taBtnFabLabeledPrimary, TaBtnFabLabeledPrimary');
  Vue.component('taBtnFabLabeledSecondary, TaBtnFabLabeledSecondary');
  Vue.component('taBtnFabPrimary, TaBtnFabPrimary');
  Vue.component('taBtnFabSecondary, TaBtnFabSecondary');
  Vue.component('taBtnGroupOutlined, TaBtnGroupOutlined');
  Vue.component('taBtnGroupPrimary, TaBtnGroupPrimary');
  Vue.component('taBtnGroupRounded, TaBtnGroupRounded');
  Vue.component('taBtnNextOutlined, TaBtnNextOutlined');
  Vue.component('taBtnNextOutlinedLarge, TaBtnNextOutlinedLarge');
  Vue.component('taBtnNextOutlinedSmall, TaBtnNextOutlinedSmall');
  Vue.component('taBtnNextPrimary, TaBtnNextPrimary');
  Vue.component('taBtnNextPrimaryLarge, TaBtnNextPrimaryLarge');
  Vue.component('taBtnNextPrimarySmall, TaBtnNextPrimarySmall');
  Vue.component('taBtnNextSecondary, TaBtnNextSecondary');
  Vue.component('taBtnNextSecondaryLarge, TaBtnNextSecondaryLarge');
  Vue.component('taBtnNextSecondarySmall, TaBtnNextSecondarySmall');
  Vue.component('taBtnRefreshOutlined, TaBtnRefreshOutlined');
  Vue.component('taBtnRefreshOutlinedLarge, TaBtnRefreshOutlinedLarge');
  Vue.component('taBtnRefreshOutlinedSmall, TaBtnRefreshOutlinedSmall');
  Vue.component('taBtnRefreshPrimary, TaBtnRefreshPrimary');
  Vue.component('taBtnRefreshPrimaryLarge, TaBtnRefreshPrimaryLarge');
  Vue.component('taBtnRefreshPrimarySmall, TaBtnRefreshPrimarySmall');
  Vue.component('taBtnRefreshSecondary, TaBtnRefreshSecondary');
  Vue.component('taBtnRefreshSecondaryLarge, TaBtnRefreshSecondaryLarge');
  Vue.component('taBtnRefreshSecondarySmall, TaBtnRefreshSecondarySmall');
  Vue.component('taBtnOutlined, TaBtnOutlined');
  Vue.component('taBtnOutlinedLarge, TaBtnOutlinedLarge');
  Vue.component('taBtnOutlinedSmall, TaBtnOutlinedSmall');
  Vue.component('taBtnPrimary, TaBtnPrimary');
  Vue.component('taBtnPrimaryLarge, TaBtnPrimaryLarge');
  Vue.component('taBtnPrimarySmall, TaBtnPrimarySmall');
  Vue.component('taBtnSecondary, TaBtnSecondary');
  Vue.component('taBtnSecondaryLarge, TaBtnSecondaryLarge');
  Vue.component('taBtnSecondarySmall, TaBtnSecondarySmall');
  Vue.component('taBtnUploadIconDelete, TaBtnUploadIconDelete');
  Vue.component('taBtnUploadIconOutlined, TaBtnUploadIconOutlined');
  Vue.component('taBtnUploadIconPrimary, TaBtnUploadIconPrimary');
  Vue.component('taBtnUploadIconSecondary, TaBtnUploadIconSecondary');
  Vue.component('taCard, TaCard');
  Vue.component('taCardDetail, TaCardDetail');
  Vue.component('taCardDetailSeal, TaCardDetailSeal');
  Vue.component('taCardDetailSecundary, TaCardDetailSecundary');
  Vue.component('taCardInfo, TaCardInfo');
  Vue.component('taCardOption, TaCardOption');
  Vue.component('taCardOptionGroup, TaCardOptionGroup');
  Vue.component('taCheckboxPrimary, TaCheckboxPrimary');
  Vue.component('taCheckboxSecundary, TaCheckboxSecundary');
  Vue.component('taChip, TaChip');
  Vue.component('taChipOutlined, TaChipOutlined');
  Vue.component('taChipPrimary, TaChipPrimary');
  Vue.component('taChipPrimaryOutlined, TaChipPrimaryOutlined');
  Vue.component('taChipSecondary, TaChipSecondary');
  Vue.component('taChipSecondaryOutlined, TaChipSecondaryOutlined');
  Vue.component('taCustomDataTable, TaCustomDataTable');
  Vue.component('taDialogConfirmation, TaDialogConfirmation');
  Vue.component('taDialogList, TaDialogList');
  Vue.component('taListInfo, TaListInfo');
  Vue.component('taLoaderPrimary, TaLoaderPrimary');
  Vue.component('taLoaderPrimaryLarge, TaLoaderPrimaryLarge');
  Vue.component('taLoaderPrimarySmall, TaLoaderPrimarySmall');
  Vue.component('taLoaderSecondary, TaLoaderSecondary');
  Vue.component('taLoaderSecondaryLarge, TaLoaderSecondaryLarge');
  Vue.component('taLoaderSecondarySmall, TaLoaderSecondarySmall');
  Vue.component('taLocation, TaLocation');
  Vue.component('taPaginationPrimary, TaPaginationPrimary');
  Vue.component('taPaginationSecundary, TaPaginationSecundary');
  Vue.component('taProgressBar, TaProgressBar');
  Vue.component('taRadioButton, TaRadioButton');
  Vue.component('taSealError, TaSealError');
  Vue.component('taSealInfo, TaSealInfo');
  Vue.component('taSealPrimary, TaSealPrimary');
  Vue.component('taSealSuccess, TaSealSuccess');
  Vue.component('taSealUnavailable, TaSealUnavailable');
  Vue.component('taSealWarning, TaSealWarning');
  Vue.component('taSelect, TaSelect');
  Vue.component('taSelectMultiple, TaSelectMultiple');
  Vue.component('taSlider, TaSlider');
  Vue.component('taSliderRange, TaSliderRange');
  Vue.component('taSteps, TaSteps');
  Vue.component('taSwitch, TaSwitch');
  Vue.component('taSwitchBar, TaSwitchBar');
  Vue.component('taTabBar, TaTabBar');
  Vue.component('taTabsPrimary, TaTabsPrimary');
  Vue.component('taTabsSecundary, TaTabsSecundary');
  Vue.component('taTextField, TaTextField');
  Vue.component('taTextFieldCompanyDoc, TaTextFieldCompanyDoc');
  Vue.component('tatextFieldCurrency, TatextFieldCurrency');
  Vue.component('taTextFieldPersonalDoc, TaTextFieldPersonalDoc');
  Vue.component('taTextFieldZipCode, TaTextFieldZipCode');
  Vue.component('taToolBar, TaToolBar');
  Vue.component('taValueControl, TaValueControl');
  Vue.component('taValueControlOutlined, TaValueControlOutlined');
  Vue.component('taAppTollbar, TaAppTollbar');
  Vue.component('taDatepicker, TaDatepicker');
  Vue.component('taSnackbar, TaSnackbar');
  Vue.component('taDrawer, TaDrawer');
}

// Create module definition for Vue.use()
const plugin = {
  install,
};

// Auto-install when vue is found (eg. in browser via <script> tag)
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// To allow use as module (npm/webpack/etc.) export component
export {
  TaAccordion,
  TaAddPrimaryLabeled,
  TaAddSuccessLabeled,
  TaAlertPrimaryActionError,
  TaAlertPrimaryActionInfo,
  TaAlertPrimaryActionSuccess,
  TaAlertPrimaryError,
  TaAlertPrimaryInfo,
  TaAlertPrimarySuccess,
  TaAlertSecondaryActionError,
  TaAlertSecondaryActionInfo,
  TaAlertSecondaryActionSuccess,
  TaAlertSecondaryError,
  TaAlertSecondaryInfo,
  TaAlertSecondarySuccess,
  TaAvaliation,
  Avatar,
  TaAvatar,
  TaAvatarDetail,
  TaBreadcrumb,
  TaBtnAddOutlined,
  TaBtnAddOutlinedLarge,
  TaBtnAddOutlinedSmall,
  TaBtnAddPrimary,
  TaBtnAddPrimaryLarge,
  TaBtnAddPrimarySmall,
  TaBtnAddSecondary,
  TaBtnAddSecondaryLarge,
  TaBtnAddSecondarySmall,
  TaBtnBackButton,
  TaBtnDownloadIconDelete,
  TaBtnDownloadIconOutlined,
  TaBtnDownloadIconPrimary,
  TaBtnDownloadIconSecondary,
  TaBtnDownloadOutlined,
  TaBtnDownloadOutlinedLarge,
  TaBtnDownloadOutlinedSmall,
  TaBtnDownloadPrimary,
  TaBtnDownloadPrimaryLarge,
  TaBtnDownloadPrimarySmall,
  TaBtnDownloadSecondary,
  TaBtnDownloadSecondaryLarge,
  TaBtnDownloadSecondarySmall,
  TaBtnDropDownOutlined,
  TaBtnDropDownPrimary,
  TaBtnFabLabeledPrimary,
  TaBtnFabLabeledSecondary,
  TaBtnFabPrimary,
  TaBtnFabSecondary,
  TaBtnGroupOutlined,
  TaBtnGroupPrimary,
  TaBtnGroupRounded,
  TaBtnNextOutlined,
  TaBtnNextOutlinedLarge,
  TaBtnNextOutlinedSmall,
  TaBtnNextPrimary,
  TaBtnNextPrimaryLarge,
  TaBtnNextPrimarySmall,
  TaBtnNextSecondary,
  TaBtnNextSecondaryLarge,
  TaBtnNextSecondarySmall,
  TaBtnRefreshOutlined,
  TaBtnRefreshOutlinedLarge,
  TaBtnRefreshOutlinedSmall,
  TaBtnRefreshPrimary,
  TaBtnRefreshPrimaryLarge,
  TaBtnRefreshPrimarySmall,
  TaBtnRefreshSecondary,
  TaBtnRefreshSecondaryLarge,
  TaBtnRefreshSecondarySmall,
  TaBtnOutlined,
  TaBtnOutlinedLarge,
  TaBtnOutlinedSmall,
  TaBtnPrimary,
  TaBtnPrimaryLarge,
  TaBtnPrimarySmall,
  TaBtnSecondary,
  TaBtnSecondaryLarge,
  TaBtnSecondarySmall,
  TaBtnUploadIconDelete,
  TaBtnUploadIconOutlined,
  TaBtnUploadIconPrimary,
  TaBtnUploadIconSecondary,
  TaCard,
  TaCardDetail,
  TaCardDetailSeal,
  TaCardDetailSecundary,
  TaCardInfo,
  TaCardOption,
  TaCardOptionGroup,
  TaCheckboxPrimary,
  TaCheckboxSecundary,
  TaChip,
  TaChipOutlined,
  TaChipPrimary,
  TaChipPrimaryOutlined,
  TaChipSecondary,
  TaChipSecondaryOutlined,
  TaCustomDataTable,
  TaDialogConfirmation,
  TaDialogList,
  TaListInfo,
  TaLoaderPrimary,
  TaLoaderPrimaryLarge,
  TaLoaderPrimarySmall,
  TaLoaderSecondary,
  TaLoaderSecondaryLarge,
  TaLoaderSecondarySmall,
  TaLocation,
  TaPaginationPrimary,
  TaPaginationSecundary,
  TaProgressBar,
  TaRadioButton,
  TaSealError,
  TaSealInfo,
  TaSealPrimary,
  TaSealSuccess,
  TaSealUnavailable,
  TaSealWarning,
  TaSelect,
  TaSelectMultiple,
  TaSlider,
  TaSliderRange,
  TaSteps,
  TaSwitch,
  TaSwitchBar,
  TaTabBar,
  TaTabsPrimary,
  TaTabsSecundary,
  TaTextField,
  TaTextFieldCompanyDoc,
  TatextFieldCurrency,
  TaTextFieldPersonalDoc,
  TaTextFieldZipCode,
  TaToolBar,
  TaValueControl,
  TaValueControlOutlined,
  TaDatepicker,
  TaDrawer,
  TaAppToolbar,
  TaSnackbar,
};
