import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Design Sistem',
    component: () => import('../views/Layout.vue'),
    meta: { requiresAuth: true },
    children: [
      {
        path: '/',
        name: 'HomePage',
        component: () => import('../views/HomePage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/btn-primary',
        name: 'TaBtnPage',
        component: () => import('../views/TaBtnPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/accordion',
        name: 'TaAccordionPage',
        component: () => import('../views/TaAccordionPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/tabs',
        name: 'TaTabsPage',
        component: () => import('../views/TaTabsPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/snackbar',
        name: 'TaSnackbar',
        component: () => import('../views/TaSnackbarPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/steps',
        name: 'TaStepsPage',
        component: () => import('../views/TaStepsPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/text-field',
        name: 'TaTextFieldPage',
        component: () => import('../views/TaTextFieldPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/checkbox',
        name: 'TaCheckbox',
        component: () => import('../views/TaCheckboxPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/chip',
        name: 'TaChip',
        component: () => import('../views/TaChipPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/radio-button',
        name: 'TaRadioButton',
        component: () => import('../views/TaRadioButtonPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/switch',
        name: 'TaSwitch',
        component: () => import('../views/TaSwitchPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/select',
        name: 'TaSelect',
        component: () => import('../views/TaSelectPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/table',
        name: 'TaTablePage',
        component: () => import('../views/TaTablePage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/avaliation',
        name: 'TaAvaliationPage',
        component: () => import('../views/TaAvaliationPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/datepicker',
        name: 'TaDatepicker',
        component: () => import('../views/TaDatepickerPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/slider',
        name: 'TaSlidersPage',
        component: () => import('../views/TaSliderPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/seal',
        name: 'TaSealPage',
        component: () => import('../views/TaSealPages.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/alerts',
        name: 'TaAlertsPage',
        component: () => import('../views/TaAlertsPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/cards',
        name: 'TaCardPage',
        component: () => import('../views/TaCardPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/avatar',
        name: 'TaAvatar',
        component: () => import('../views/TaAvatarPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/loader',
        name: 'TaLoader',
        component: () => import('../views/TaLoaderPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/value-control',
        name: 'TaValueControlPage',
        component: () => import('../views/TaValueControlPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/bar',
        name: 'TaBarPage',
        component: () => import('../views/TaBarPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/list-info',
        name: 'TaListInfoPage',
        component: () => import('../views/TaListInfoPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/dialog',
        name: 'TaDialog',
        component: () => import('../views/TaDialogPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/location',
        name: 'TaLocationPage',
        component: () => import('../views/TaLocationPage.vue'),
        meta: { requiresAuth: true },
      },
      {
        path: '/breascrumb',
        name: 'TaBreadcrumbPage',
        component: () => import('../views/TaBreadcrumbPage.vue'),
        meta: { requiresAuth: true },
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
