import TaAvatar from './TaAvatar.vue';

export default {
  title: 'Design System/TaAvatar/TaAvatar',
  component: TaAvatar,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAvatar },
  template: `<v-card class="pa-5">
    <v-row>
      <v-col>
        <TaAvatar
          :avatarSize="40"
          :avatarType="'label'"
          :avatarLabel="'TA'"
        />
      </v-col>
      <v-col>
        <TaAvatar
          :avatarSize="40"
          :avatarType="'image'"
          :avatarImage="'https://cdn.vuetifyjs.com/images/john.jpg'"
        />
      </v-col>
      <v-col>
        <TaAvatar
          :avatarSize="40"
        />
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <TaAvatar
          :type="'badgeType'"
          :profileStatus="'Online'"
          :avatarSize="40"
          :avatarType="'label'"
          :avatarLabel="'TA'"
        />
      </v-col>
      <v-col>
        <TaAvatar
          :type="'badgeType'"
          :profileStatus="'offline'"
          :avatarSize="40"
          :avatarType="'image'"
          :avatarImage="'https://cdn.vuetifyjs.com/images/john.jpg'"
        />
      </v-col>
      <v-col>
        <TaAvatar
          :type="'badgeType'"
          :profileStatus="'Online'"
          :avatarSize="40"
        />
      </v-col>
    </v-row>
  </v-card>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Adicionar perfil',
};
