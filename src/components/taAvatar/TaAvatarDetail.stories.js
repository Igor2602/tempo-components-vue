import TaAvatarDetail from './TaAvatarDetail.vue';

export default {
  title: 'Design System/TaAvatar/TaAvatarDetail',
  component: TaAvatarDetail,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAvatarDetail },
  template: `<v-card class="pa-5">
  <v-row>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarTitle="avatarTitle"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarTitle="avatarTitle"
          :avatarSubtitle="avatarSubtitle"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarType="avatarTypeImage"
          :avatarTitle="avatarTitle"
          :avatarImage="avatarImage"
        />
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarType="avatarTypeImage"
          :avatarTitle="avatarTitle"
          :avatarSubtitle="avatarSubtitle"
          :avatarImage="avatarImage"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarType="avatarTypeLabel"
          :avatarTitle="avatarTitle"
          :avatarLabel="avatarLabel"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarTitle="avatarTitle"
        />
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarType="avatarTypeLabel"
          :avatarTitle="avatarTitle"
          :avatarLabel="avatarLabel"
          :avatarSubtitle="avatarSubtitle"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarTitle="avatarTitle"
          :avatarSubtitle="avatarSubtitle"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarType="avatarTypeImage"
          :avatarTitle="avatarTitle"
          :avatarImage="avatarImage"
        />
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarType="avatarTypeImage"
          :avatarTitle="avatarTitle"
          :avatarSubtitle="avatarSubtitle"
          :avatarImage="avatarImage"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarType="avatarTypeLabel"
          :avatarTitle="avatarTitle"
          :avatarLabel="avatarLabel"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarType="avatarTypeLabel"
          :avatarTitle="avatarTitle"
          :avatarLabel="avatarLabel"
          :avatarSubtitle="avatarSubtitle"
        />
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarTitle="avatarTitle"
          :avatarEdit="true"
          @profile-edit="onClick"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarTitle="avatarTitle"
          :avatarSubtitle="avatarSubtitle"
          :avatarEdit="true"
          @profile-edit="onClick"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarType="avatarTypeImage"
          :avatarTitle="avatarTitle"
          :avatarImage="avatarImage"
          :avatarEdit="true"
          @profile-edit="onClick"
        />
      </v-col>
    </v-row>

    <v-row>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarType="avatarTypeImage"
          :avatarTitle="avatarTitle"
          :avatarSubtitle="avatarSubtitle"
          :avatarImage="avatarImage"
          :avatarEdit="true"
          @profile-edit="onClick"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :avatarSize="avatarSize"
          :avatarType="avatarTypeLabel"
          :avatarTitle="avatarTitle"
          :avatarLabel="avatarLabel"
          :avatarEdit="true"
          @profile-edit="onClick"
        />
      </v-col>
      <v-col>
        <TaAvatarDetail
          :type="type"
          :profileStatus="profileStatus"
          :avatarSize="avatarSize"
          :avatarType="avatarTypeLabel"
          :avatarTitle="avatarTitle"
          :avatarLabel="avatarLabel"
          :avatarSubtitle="avatarSubtitle"
          :avatarEdit="true"
          @profile-edit="onClick"
        />
      </v-col>
    </v-row>
  </v-card>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  avatarSize: 40,
  avatarTitle: 'Tatiana Shmayluk',
  avatarSubtitle: 'Cliente',
  avatarImage: 'https://cdn.vuetifyjs.com/images/john.jpg',
  avatarTypeImage: 'image',
  avatarTypeLabel: 'label',
  avatarLabel: 'TA',
  type: 'badgeType',
  profileStatus: 'Online',
};
