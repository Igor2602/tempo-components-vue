import TaAccordion from './TaAccordion.vue';

export default {
  title: 'Design System/TaAccordion/TaAccordion',
  component: TaAccordion,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAccordion },
  template: `<TaAccordion
    :items="items"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  items: [
    {
      title: 'Título',
      subtitle: 'Subtítulo',
      text: 'Phasellus volutpat congue nibh ut ullamcorper. Vestibulum cursus sapien at est euismod vehicula. Donec cursus fringilla velit et hendrerit.',
    },
    {
      title: 'Título',
      subtitle: 'Subtítulo',
      text: 'Phasellus volutpat congue nibh ut ullamcorper. Vestibulum cursus sapien at est euismod vehicula. Donec cursus fringilla velit et hendrerit.',
    },
  ],
};
