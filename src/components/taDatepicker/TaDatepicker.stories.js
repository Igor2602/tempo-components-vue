import TaDatepicker from './TaDatepicker.vue';

export default {
  title: 'Design System/TaDatepicker/TaDatepicker',
  component: TaDatepicker,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaDatepicker },
  template: `<TaDatepicker
    :dialog="dialog"
    :valueDate="value"
    :firstDay="firstDay"
    :weekOnlyProps="weekOnlyProps"
    :nowDateProps="nowDateProps"
    :disabled="disabled"
    :readonly="readonly"
    :locale="locale"
    :formatDate="formatDate"
    @onChangeDate="onClick"
    @cancelPicker="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  dialog: true,
  firstDay: 0,
  weekOnlyProps: false,
  nowDateProps: false,
  disabled: false,
  readonly: false,
  locale: 'pt-BR',
  formatDate: 'DD/MM/YYYY',
  value:
  (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
};
