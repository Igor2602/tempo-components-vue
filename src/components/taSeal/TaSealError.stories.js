import TaSealError from './TaSealError.vue';

export default {
  title: 'Design System/TaSeal/TaSealError',
  component: TaSealError,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSealError },
  template: `<TaSealError
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Emergencial',
};
