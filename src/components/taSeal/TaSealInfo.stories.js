import TaSealInfo from './TaSealInfo.vue';

export default {
  title: 'Design System/TaSeal/TaSealInfo',
  component: TaSealInfo,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSealInfo },
  template: `<TaSealInfo
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: '18',
};
