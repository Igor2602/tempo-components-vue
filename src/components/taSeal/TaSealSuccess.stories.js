import TaSealSuccess from './TaSealSuccess.vue';

export default {
  title: 'Design System/TaSeal/TaSealSuccess',
  component: TaSealSuccess,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSealSuccess },
  template: `<TaSealSuccess
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Concluído',
};
