import TaSealPrimary from './TaSealPrimary.vue';

export default {
  title: 'Design System/TaSeal/TaSealPrimary',
  component: TaSealPrimary,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSealPrimary },
  template: `<TaSealPrimary
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Serviço',
};
