import TaSealUnavailable from './TaSealUnavailable.vue';

export default {
  title: 'Design System/TaSeal/TaSealUnavailable',
  component: TaSealUnavailable,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSealUnavailable },
  template: `<TaSealUnavailable
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Indisponível',
};
