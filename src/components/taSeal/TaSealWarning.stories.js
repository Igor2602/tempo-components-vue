import TaSealWarning from './TaSealWarning.vue';

export default {
  title: 'Design System/TaSeal/TaSealWarning',
  component: TaSealWarning,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSealWarning },
  template: `<TaSealWarning
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Aguardando',
};
