import TaAvaliation from './TaAvaliation.vue';

export default {
  title: 'Design System/TaAvaliation/TaAvaliation',
  component: TaAvaliation,
  argTypes: {
    input: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAvaliation },
  template: `<TaAvaliation
    v-model="rating"
    :AvaliationSize="avaliationSize"
    half-increments
    @input="input($event)"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  rating: 3.5,
  avaliationSize: '30',
};
