import TaSteps from './TaSteps.vue';

export default {
  title: 'Design System/TaSteps/TaSteps',
  component: TaSteps,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSteps },
  template: `<TaSteps
    :items="items"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  items: [
    {
      label: 'Em rota',
      step: 1,
      concluded: true,
      active: false,
    },
    {
      label: 'Chegada',
      step: 2,
      concluded: true,
      active: false,
    },
    {
      label: 'Serviço',
      step: 3,
      concluded: false,
      active: true,
    },
    {
      label: 'Finalização',
      step: 4,
      concluded: false,
      active: false,
    },
  ],
};
