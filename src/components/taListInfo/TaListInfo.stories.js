import TaListInfo from './TaListInfo.vue';

export default {
  title: 'Design System/TaListInfo/TaListInfo',
  component: TaListInfo,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaListInfo },
  template: `<TaListInfo
    :items="items"
    @edit-item="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  items: [
    { title: 'Nome', text: 'Tatiana Shmayluk', icon: 'mdi-pencil' },
    { title: 'E-mail', text: 'tati_booya@gmail.com', icon: 'mdi-pencil' },
    { title: 'Telefone', text: '11999999998', icon: 'mdi-pencil' },
  ],
};
