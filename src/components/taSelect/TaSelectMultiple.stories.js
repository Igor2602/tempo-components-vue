import TaSelectMultiple from './TaSelectMultiple.vue';

export default {
  title: 'Design System/TaSelect/TaSelectMultiple',
  component: TaSelectMultiple,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSelectMultiple },
  template: `<TaSelectMultiple
    v-model="selectModelMult"
    :label="label"
    item-text="value"
    item-value="key"
    :items="items"
    @change="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  selectModel: null,
  label: 'Cidade',
  items: [
    {
      key: 0, value: 'São Lourenço da Serra',
    },
    {
      key: 1, value: 'São Luiz do Paraitinga',
    },
    {
      key: 2, value: 'São Manuel',
    },
    {
      key: 3, value: 'São Miguel Arcanjo',
    },
    {
      key: 4, value: 'São Paulo',
    },
    {
      key: 5, value: 'São Pedro',
    },
    {
      key: 6, value: 'São Roque',
    },
    {
      key: 7, value: 'São Sebastião',
    },
  ],
};
