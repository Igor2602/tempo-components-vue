import TaSelect from './TaSelect.vue';

export default {
  title: 'Design System/TaSelect/TaSelect',
  component: TaSelect,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSelect },
  template: `<TaSelect
    v-model="selectModel"
    item-text="value"
    item-value="key"
    :label="label"
    :items="items"
    @change="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  selectModel: null,
  label: 'Cidade',
  items: [
    {
      key: 0, value: 'São Lourenço da Serra',
    },
    {
      key: 1, value: 'São Luiz do Paraitinga',
    },
    {
      key: 2, value: 'São Manuel',
    },
    {
      key: 3, value: 'São Miguel Arcanjo',
    },
    {
      key: 4, value: 'São Paulo',
    },
    {
      key: 5, value: 'São Pedro',
    },
    {
      key: 6, value: 'São Roque',
    },
    {
      key: 7, value: 'São Sebastião',
    },
  ],
};
