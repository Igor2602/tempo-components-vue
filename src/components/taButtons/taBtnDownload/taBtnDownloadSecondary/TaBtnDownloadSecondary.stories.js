import TaBtnDownloadSecondary from './TaBtnDownloadSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TaBtnDownloadSecondary/TaBtnDownloadSecondary',
  component: TaBtnDownloadSecondary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadSecondary },
  template: `<TaBtnDownloadSecondary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
