import TaBtnDownloadSecondaryLarge from './TaBtnDownloadSecondaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TaBtnDownloadSecondary/TaBtnDownloadSecondaryLarge',
  component: TaBtnDownloadSecondaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadSecondaryLarge },
  template: `<TaBtnDownloadSecondaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
