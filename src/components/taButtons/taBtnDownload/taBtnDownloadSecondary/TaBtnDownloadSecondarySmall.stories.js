import TaBtnDownloadSecondarySmall from './TaBtnDownloadSecondarySmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TaBtnDownloadSecondary/TaBtnDownloadSecondarySmall',
  component: TaBtnDownloadSecondarySmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadSecondarySmall },
  template: `<TaBtnDownloadSecondarySmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
