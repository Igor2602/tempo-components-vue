import TaBtnDownloadPrimarySmall from './TaBtnDownloadPrimarySmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TaBtnDownloadPrimary/TaBtnDownloadPrimarySmall',
  component: TaBtnDownloadPrimarySmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadPrimarySmall },
  template: `<TaBtnDownloadPrimarySmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
