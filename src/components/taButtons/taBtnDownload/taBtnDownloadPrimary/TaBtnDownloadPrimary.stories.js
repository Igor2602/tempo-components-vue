import TaBtnDownloadPrimary from './TaBtnDownloadPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TaBtnDownloadPrimary/TaBtnDownloadPrimary',
  component: TaBtnDownloadPrimary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadPrimary },
  template: `<TaBtnDownloadPrimary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
