import TaBtnDownloadOutlinedLarge from './TaBtnDownloadOutlinedLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TabtnDownloadOutlined/TaBtnDownloadOutlinedLarge',
  component: TaBtnDownloadOutlinedLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadOutlinedLarge },
  template: `<TaBtnDownloadOutlinedLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
