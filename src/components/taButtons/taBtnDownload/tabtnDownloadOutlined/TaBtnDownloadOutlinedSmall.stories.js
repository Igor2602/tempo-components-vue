import TaBtnDownloadOutlinedSmall from './TaBtnDownloadOutlinedSmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownload/TabtnDownloadOutlined/TaBtnDownloadOutlinedSmall',
  component: TaBtnDownloadOutlinedSmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadOutlinedSmall },
  template: `<TaBtnDownloadOutlinedSmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
