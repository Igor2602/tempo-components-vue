import TaBtnFabLabeledSecondary from './TaBtnFabLabeledSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnFab/Labeled/TaBtnFabLabeledSecondary',
  component: TaBtnFabLabeledSecondary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
  disabled: {
    control: { type: 'select' },
    options: [true, false],
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnFabLabeledSecondary },
  template: `<TaBtnFabLabeledSecondary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
