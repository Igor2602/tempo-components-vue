import TaBtnFabLabeledPrimary from './TaBtnFabLabeledPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnFab/Labeled/TaBtnFabLabeledPrimary',
  component: TaBtnFabLabeledPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
  disabled: {
    control: { type: 'select' },
    options: [true, false],
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnFabLabeledPrimary },
  template: `<TaBtnFabLabeledPrimary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
