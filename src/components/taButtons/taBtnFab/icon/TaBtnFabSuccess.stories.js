import TaBtnFabSuccess from './TaBtnFabSuccess.vue';

export default {
  title: 'Design System/TaButtons/TaBtnFab/Icon/TaBtnFabSuccess',
  component: TaBtnFabSuccess,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnFabSuccess },
  template: `<TaBtnFabSuccess
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
