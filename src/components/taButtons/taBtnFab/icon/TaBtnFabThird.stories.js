import TaBtnFabThird from './TaBtnFabThird.vue';

export default {
  title: 'Design System/TaButtons/TaBtnFab/Icon/TaBtnFabThird',
  component: TaBtnFabThird,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnFabThird },
  template: `<TaBtnFabThird
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
