import TaBtnFabPrimary from './TaBtnFabPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnFab/Icon/TaBtnFabPrimary',
  component: TaBtnFabPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnFabPrimary },
  template: `<TaBtnFabPrimary
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
