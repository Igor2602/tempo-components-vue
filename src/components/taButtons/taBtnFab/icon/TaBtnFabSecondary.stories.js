import TaBtnFabSecondary from './TaBtnFabSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnFab/Icon/TaBtnFabSecondary',
  component: TaBtnFabSecondary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnFabSecondary },
  template: `<TaBtnFabSecondary
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
