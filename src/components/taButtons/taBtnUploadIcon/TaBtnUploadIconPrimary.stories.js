import TaBtnUploadIconPrimary from './TaBtnUploadIconPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnUploadIcon/TaBtnUploadIconPrimary',
  component: TaBtnUploadIconPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnUploadIconPrimary },
  template: `<TaBtnUploadIconPrimary
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
