import TaBtnUploadIconDelete from './TaBtnUploadIconDelete.vue';

export default {
  title: 'Design System/TaButtons/TaBtnUploadIcon/TaBtnUploadIconDelete',
  component: TaBtnUploadIconDelete,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnUploadIconDelete },
  template: `<TaBtnUploadIconDelete
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
