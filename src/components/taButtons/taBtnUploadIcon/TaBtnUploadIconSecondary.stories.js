import TaBtnUploadIconSecondary from './TaBtnUploadIconSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnUploadIcon/TaBtnUploadIconSecondary',
  component: TaBtnUploadIconSecondary,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnUploadIconSecondary },
  template: `<TaBtnUploadIconSecondary
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
