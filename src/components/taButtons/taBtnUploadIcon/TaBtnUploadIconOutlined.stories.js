import TaBtnUploadIconOutlined from './TaBtnUploadIconOutlined.vue';

export default {
  title: 'Design System/TaButtons/TaBtnUploadIcon/TaBtnUploadIconOutlined',
  component: TaBtnUploadIconOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnUploadIconOutlined },
  template: `<TaBtnUploadIconOutlined
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
