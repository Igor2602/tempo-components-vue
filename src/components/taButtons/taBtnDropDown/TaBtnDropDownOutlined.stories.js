import TaBtnDropDownOutlined from './TaBtnDropDownOutlined.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDropDown/TaBtnDropDownOutlined',
  component: TaBtnDropDownOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDropDownOutlined },
  template: `<TaBtnDropDownOutlined
    :items="taBtnDropDown"
    :label="label"
    @select-item="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  taBtnDropDown: [
    { title: 'Click Me 1' },
    { title: 'Click Me 2' },
    { title: 'Click Me 3' },
    { title: 'Click Me 4' },
  ],
};
