import TaBtnDropDownPrimary from './TaBtnDropDownPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDropDown/TaBtnDropDownPrimary',
  component: TaBtnDropDownPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDropDownPrimary },
  template: `<TaBtnDropDownPrimary
    :items="taBtnDropDown"
    :label="label"
    @select-item="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  taBtnDropDown: [
    { title: 'Click Me 1' },
    { title: 'Click Me 2' },
    { title: 'Click Me 3' },
    { title: 'Click Me 4' },
  ],
};
