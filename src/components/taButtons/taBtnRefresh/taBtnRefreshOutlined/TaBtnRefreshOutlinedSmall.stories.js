import TaBtnRefreshOutlinedSmall from './TaBtnRefreshOutlinedSmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnRefresh/TaBtnRefreshOutlined/TaBtnRefreshOutlinedSmall',
  component: TaBtnRefreshOutlinedSmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnRefreshOutlinedSmall },
  template: `<TaBtnRefreshOutlinedSmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
