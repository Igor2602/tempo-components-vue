import TaBtnRefreshOutlinedLarge from './TaBtnRefreshOutlinedLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnRefresh/TaBtnRefreshOutlined/TaBtnRefreshOutlinedLarge',
  component: TaBtnRefreshOutlinedLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnRefreshOutlinedLarge },
  template: `<TaBtnRefreshOutlinedLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
