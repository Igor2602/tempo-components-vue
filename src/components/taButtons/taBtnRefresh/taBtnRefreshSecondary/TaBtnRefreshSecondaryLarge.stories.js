import TaBtnRefreshSecondaryLarge from './TaBtnRefreshSecondaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnRefresh/TaBtnRefreshSecondary/TaBtnRefreshSecondaryLarge',
  component: TaBtnRefreshSecondaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnRefreshSecondaryLarge },
  template: `<TaBtnRefreshSecondaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
