import TaBtnRefreshSecondarySmall from './TaBtnRefreshSecondarySmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnRefresh/TaBtnRefreshSecondary/TaBtnRefreshSecondarySmall',
  component: TaBtnRefreshSecondarySmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnRefreshSecondarySmall },
  template: `<TaBtnRefreshSecondarySmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
