import TaBtnRefreshPrimaryLarge from './TaBtnRefreshPrimaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnRefresh/TaBtnRefreshPrimary/TaBtnRefreshPrimaryLarge',
  component: TaBtnRefreshPrimaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnRefreshPrimaryLarge },
  template: `<TaBtnRefreshPrimaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'button',
  disabled: false,
};
