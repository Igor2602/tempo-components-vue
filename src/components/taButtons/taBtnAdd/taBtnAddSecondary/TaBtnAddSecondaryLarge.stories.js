import TaBtnAddSecondaryLarge from './TaBtnAddSecondaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/TaBtnAddSecondary/TaBtnAddSecondaryLarge',
  component: TaBtnAddSecondaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnAddSecondaryLarge },
  template: `<TaBtnAddSecondaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
