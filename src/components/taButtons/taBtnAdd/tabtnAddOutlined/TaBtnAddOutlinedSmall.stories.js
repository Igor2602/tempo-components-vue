import TaBtnAddOutlinedSmall from './TaBtnAddOutlinedSmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/TaBtnAddOutlined/TaBtnAddOutlinedSmall',
  component: TaBtnAddOutlinedSmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnAddOutlinedSmall },
  template: `<TaBtnAddOutlinedSmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
