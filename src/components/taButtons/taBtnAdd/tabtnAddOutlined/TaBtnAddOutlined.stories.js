import TaBtnAddOutlined from './TaBtnAddOutlined.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/TaBtnAddOutlined/TaBtnAddOutlined',
  component: TaBtnAddOutlined,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnAddOutlined },
  template: `<TaBtnAddOutlined
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
