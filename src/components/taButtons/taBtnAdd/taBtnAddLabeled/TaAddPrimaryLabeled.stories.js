import TaAddPrimaryLabeled from './TaAddPrimaryLabeled.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/taBtnAddLabeled/TaAddPrimaryLabeled',
  component: TaAddPrimaryLabeled,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAddPrimaryLabeled },
  template: `<TaAddPrimaryLabeled
    :label="label"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Adicionar perfil',
};
