import TaAddSuccessLabeled from './TaAddSuccessLabeled.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/taBtnAddLabeled/TaAddSuccessLabeled',
  component: TaAddSuccessLabeled,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAddSuccessLabeled },
  template: `<TaAddSuccessLabeled
    :label="label"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Adicionar perfil',
};
