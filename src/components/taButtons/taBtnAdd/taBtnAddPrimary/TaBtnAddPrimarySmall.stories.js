import TaBtnAddPrimarySmall from './TaBtnAddPrimarySmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/TaBtnAddPrimary/TaBtnAddPrimarySmall',
  component: TaBtnAddPrimarySmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnAddPrimarySmall },
  template: `<TaBtnAddPrimarySmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
