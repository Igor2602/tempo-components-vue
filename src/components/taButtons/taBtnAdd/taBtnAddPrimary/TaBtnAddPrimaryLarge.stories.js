import TaBtnAddPrimaryLarge from './TaBtnAddPrimaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/TaBtnAddPrimary/TaBtnAddPrimaryLarge',
  component: TaBtnAddPrimaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnAddPrimaryLarge },
  template: `<TaBtnAddPrimaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
