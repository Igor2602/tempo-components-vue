import TaBtnAddPrimary from './TaBtnAddPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnAdd/TaBtnAddPrimary/TaBtnAddPrimary',
  component: TaBtnAddPrimary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnAddPrimary },
  template: `<TaBtnAddPrimary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
