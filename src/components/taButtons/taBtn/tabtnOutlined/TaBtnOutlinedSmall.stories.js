import TaBtnOutlinedSmall from './TaBtnOutlinedSmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtn/TabtnOutlined/TaBtnOutlinedSmall',
  component: TaBtnOutlinedSmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnOutlinedSmall },
  template: `<TaBtnOutlinedSmall
    ::label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
