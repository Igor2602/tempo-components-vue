import TaBtnSecondarySmall from './TaBtnSecondarySmall.vue';

export default {
  title: 'Design System/TaButtons/TaBtn/TaBtnSecondary/TaBtnSecondarySmall',
  component: TaBtnSecondarySmall,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnSecondarySmall },
  template: `<TaBtnSecondarySmall
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
