import TaBtnSecondary from './TaBtnSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtn/TaBtnSecondary/TaBtnSecondary',
  component: TaBtnSecondary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnSecondary },
  template: `<TaBtnSecondary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
