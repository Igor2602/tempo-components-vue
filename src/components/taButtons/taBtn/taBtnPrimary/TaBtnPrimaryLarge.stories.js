import TaBtnPrimaryLarge from './TaBtnPrimaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtn/TaBtnPrimary/TaBtnPrimaryLarge',
  component: TaBtnPrimaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnPrimaryLarge },
  template: `<TaBtnPrimaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
