import TaBtnPrimary from './TaBtnPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtn/TaBtnPrimary/TaBtnPrimary',
  component: TaBtnPrimary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnPrimary },
  template: `<TaBtnPrimary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
