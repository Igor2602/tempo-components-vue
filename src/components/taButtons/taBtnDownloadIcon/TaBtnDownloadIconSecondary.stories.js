import TaBtnDownloadIconSecondary from './TaBtnDownloadIconSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownloadIcon/TaBtnDownloadIconSecondary',
  component: TaBtnDownloadIconSecondary,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadIconSecondary },
  template: `<TaBtnDownloadIconSecondary
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
