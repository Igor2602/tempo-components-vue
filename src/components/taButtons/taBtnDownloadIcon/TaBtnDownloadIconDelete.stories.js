import TaBtnDownloadIconDelete from './TaBtnDownloadIconDelete.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownloadIcon/TaBtnDownloadIconDelete',
  component: TaBtnDownloadIconDelete,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadIconDelete },
  template: `<TaBtnDownloadIconDelete
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
