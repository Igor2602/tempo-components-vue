import TaBtnDownloadIconOutlined from './TaBtnDownloadIconOutlined.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownloadIcon/TaBtnDownloadIconOutlined',
  component: TaBtnDownloadIconOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadIconOutlined },
  template: `<TaBtnDownloadIconOutlined
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
