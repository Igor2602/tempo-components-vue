import TaBtnDownloadIconPrimary from './TaBtnDownloadIconPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnDownloadIcon/TaBtnDownloadIconPrimary',
  component: TaBtnDownloadIconPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnDownloadIconPrimary },
  template: `<TaBtnDownloadIconPrimary
    @click.native="onClick"
    :disabled="disabled"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  disabled: false,
};
