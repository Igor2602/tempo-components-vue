import TaBtnNextPrimary from './TaBtnNextPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnNext/TaBtnNextPrimary/TaBtnNextPrimary',
  component: TaBtnNextPrimary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnNextPrimary },
  template: `<TaBtnNextPrimary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
