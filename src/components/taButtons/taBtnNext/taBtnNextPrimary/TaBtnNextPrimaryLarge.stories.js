import TaBtnNextPrimaryLarge from './TaBtnNextPrimaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnNext/TaBtnNextPrimary/TaBtnNextPrimaryLarge',
  component: TaBtnNextPrimaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnNextPrimaryLarge },
  template: `<TaBtnNextPrimaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
