import TaBtnNextSecondaryLarge from './TaBtnNextSecondaryLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnNext/TaBtnNextSecondary/TaBtnNextSecondaryLarge',
  component: TaBtnNextSecondaryLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnNextSecondaryLarge },
  template: `<TaBtnNextSecondaryLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
