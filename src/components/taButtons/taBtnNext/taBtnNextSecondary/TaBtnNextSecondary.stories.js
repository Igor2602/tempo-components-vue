import TaBtnNextSecondary from './TaBtnNextSecondary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnNext/TaBtnNextSecondary/TaBtnNextSecondary',
  component: TaBtnNextSecondary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnNextSecondary },
  template: `<TaBtnNextSecondary
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
