import TaBtnNextOutlined from './TaBtnNextOutlined.vue';

export default {
  title: 'Design System/TaButtons/TaBtnNext/TaBtnNextOutlined/TaBtnNextOutlined',
  component: TaBtnNextOutlined,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnNextOutlined },
  template: `<TaBtnNextOutlined
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
