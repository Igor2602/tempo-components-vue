import TaBtnNextOutlinedLarge from './TaBtnNextOutlinedLarge.vue';

export default {
  title: 'Design System/TaButtons/TaBtnNext/TaBtnNextOutlined/TaBtnNextOutlinedLarge',
  component: TaBtnNextOutlinedLarge,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnNextOutlinedLarge },
  template: `<TaBtnNextOutlinedLarge
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'BUTTON',
  disabled: false,
};
