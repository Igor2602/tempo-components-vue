import TaBtnGroupRounded from './TaBtnGroupRounded.vue';

export default {
  title: 'Design System/TaButtons/TaBtnGroup/TaBtnGroupRounded',
  component: TaBtnGroupRounded,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnGroupRounded },
  template: `<TaBtnGroupRounded
    :items="taBtnGroup"
    v-model="toggle_multiple"
    @item-change="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  toggle_multiple: [],
  taBtnGroup: [
    {
      label: 'Button 1',
      disabled: false,
    },
    {
      label: 'Button',
      disabled: false,
    },
    {
      label: 'Button',
      disabled: false,
    },
  ],
};
