import TaBtnGroupOutlined from './TaBtnGroupOutlined.vue';

export default {
  title: 'Design System/TaButtons/TaBtnGroup/TaBtnGroupOutlined',
  component: TaBtnGroupOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnGroupOutlined },
  template: `<TaBtnGroupOutlined
    :items="taBtnGroup"
    v-model="toggle_multiple"
    @item-change="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  toggle_multiple: [],
  taBtnGroup: [
    {
      label: 'Button 1',
      disabled: false,
    },
    {
      label: 'Button',
      disabled: false,
    },
    {
      label: 'Button',
      disabled: false,
    },
  ],
};
