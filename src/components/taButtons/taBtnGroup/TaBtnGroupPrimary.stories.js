import TaBtnGroupPrimary from './TaBtnGroupPrimary.vue';

export default {
  title: 'Design System/TaButtons/TaBtnGroup/TaBtnGroupPrimary',
  component: TaBtnGroupPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnGroupPrimary },
  template: `<TaBtnGroupPrimary
    :items="taBtnGroup"
    v-model="toggle_multiple"
    @item-change="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  toggle_multiple: [],
  taBtnGroup: [
    {
      label: 'Button 1',
      disabled: false,
    },
    {
      label: 'Button',
      disabled: false,
    },
    {
      label: 'Button',
      disabled: false,
    },
  ],
};
