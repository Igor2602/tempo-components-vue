import TaBtnBackButton from './TaBtnBackButton.vue';

export default {
  title: 'Design System/TaButtons/TaBtnBackButton/TaBtnBackButton',
  component: TaBtnBackButton,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBtnBackButton },
  template: `<TaBtnBackButton
    :label="label"
    :disabled="disabled"
    @click.native="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Voltar',
  disabled: false,
};
