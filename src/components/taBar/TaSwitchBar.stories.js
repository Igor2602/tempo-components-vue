import TaSwitchBar from './TaSwitchBar.vue';

export default {
  title: 'Design System/TaBar/TaSwitchBar',
  component: TaSwitchBar,
  argTypes: {
    onClick: { action: 'clicked' },
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSwitchBar },
  template: `<div style="background: #D9D8D9; padding: 50px">
    <TaSwitchBar
    v-model="taSwitchBarPrimary"
    @click="onClick"
    :input-value="onInput"
    :icon="icon"
    :title="title"
    :subtitle="subtitle"
    :bgColor="bgColor"
  />
  </div>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  taSwitchBarPrimary: false,
  icon: 'mdi-bell',
  title: 'Ativar notificações',
  subtitle: 'Acessar localização do dispositivo',
  bgColor: '#ffffff',
};
