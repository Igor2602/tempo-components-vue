import TaProgressBar from './TaProgressBar.vue';

export default {
  title: 'Design System/TaBar/TaProgressBar',
  component: TaProgressBar,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaProgressBar },
  template: `<TaProgressBar
    :value="progressValue"
    v-model="progressValue"
    :percentageValue="progressValue"
    :percentage="percentage"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  progressValue: 50,
  percentage: true,
};
