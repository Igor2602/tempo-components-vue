import TaToolBar from './TaToolBar.vue';

export default {
  title: 'Design System/TaBar/TaToolBar',
  component: TaToolBar,
  argTypes: {
    onClick: { action: 'clicked' },
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaToolBar },
  template: `<v-row>
    <v-col cols="12" md="6">
      <TaToolBar
        :menuItems="menuItems"
        :toolbarIcon="'mdi-menu'"
        :label="label"
        :iconPrimary="'mdi-bell'"
        @toolbar-icon="onClick"
        @primary-icon="onClick"
        @select-menu="onClick"
      />
    </v-col>
    <v-col cols="12" md="6">
      <TaToolBar
        :menuItems="menuItems"
        :toolbarIcon="'mdi-close'"
        :label="label"
        :iconPrimary="'mdi-magnify'"
        height="52"
        @toolbar-icon="onClick"
        @primary-icon="onClick"
        @select-menu="onClick"
        @blur="onClick"
        v-on:keyup.enter="onClick"
        v-model="search"
      />
    </v-col>
    <v-col cols="12" md="6">
      <TaToolBar
        :menuItems="menuItems"
        :toolbarIcon="'mdi-arrow-left'"
        :label="label"
        :iconPrimary="'mdi-share-variant'"
        @toolbar-icon="onClick"
        @primary-icon="onClick"
        @select-menu="onClick"
      />
    </v-col>

    <v-col cols="12" md="6">
      <TaToolBar
        :menuItems="menuItems"
        :toolbarIcon="'mdi-arrow-left'"
        :label="selectLabel"
        :iconPrimary="'mdi-delete'"
        @menu="onClick"
        @primaryIcon="onClick"
        @select-menu="onClick"
      />
    </v-col>
  </v-row>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Título da tela',
  selectLabel: '2 selecionados',
  toolbarIcon: 'mdi-close',
  primaryIcon: 'mdi-magnify',
  search: null,
  menuItems: [
    {
      title: 'Option 1',
    },
    {
      title: 'Option 2',
    },
  ],
};
