import TaAppToolbar from './TaAppToolbar.vue';

export default {
  title: 'Design System/TaBar/TaAppToolbar',
  component: TaAppToolbar,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAppToolbar },
  template: `<div style="background: #D9D8D9; padding: 50px">
  <TaAppToolbar
    :profile="profile"
    :title="title"
    :btnLabel="btnLabel"
    @on-click="onClick"
    @navigate-drawer="onClick"
  />
  </div>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  profile: {
    url: 'https://cdn.vuetifyjs.com/images/john.jpg',
    name: 'Tatiana Shmayluk',
  },
  title: 'Título da página',
  btnLabel: 'Sair',
};
