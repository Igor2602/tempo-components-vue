import TaTabBar from './TaTabBar.vue';

export default {
  title: 'Design System/TaBar/TaTabBar',
  component: TaTabBar,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaTabBar },
  template: `<TaTabBar
    v-model="value"
    :items="itemsPrimary"
    @select-item="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  value: 1,
  itemsPrimary: [
    {
      title: 'Home',
      icon: 'mdi-home',
      value: 'Home',
    },
    {
      title: 'Busca',
      icon: 'mdi-magnify',
      value: 'Buscar',
    },
    {
      title: 'Agenda',
      icon: 'mdi-calendar-check',
      value: 'Agenda',
    },
    {
      title: 'Perfil',
      icon: 'mdi-account',
      value: 'Perfil',
    },
  ],
};
