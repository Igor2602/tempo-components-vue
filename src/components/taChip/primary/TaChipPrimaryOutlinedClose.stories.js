import TaChipPrimaryOutlined from './TaChipPrimaryOutlined.vue';

export default {
  title: 'Design System/TaChip/Primary/TaChipPrimaryOutlinedClose',
  component: TaChipPrimaryOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipPrimaryOutlined },
  template: `<TaChipPrimaryOutlined
    v-if="taChip"
    :label="label"
    close
    @click:close="taChip = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
  taChip: true,
};
