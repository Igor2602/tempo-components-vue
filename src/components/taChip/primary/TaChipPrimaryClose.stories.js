import TaChipPrimary from './TaChipPrimary.vue';

export default {
  title: 'Design System/TaChip/Primary/TaChipPrimaryClose',
  component: TaChipPrimary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipPrimary },
  template: `<TaChipPrimary
    v-if="taChip"
    :label="label"
    close
    @click:close="taChip = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
  taChip: true,
};
