import TaChipPrimary from './TaChipPrimary.vue';

export default {
  title: 'Design System/TaChip/Primary/TaChipPrimary',
  component: TaChipPrimary,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipPrimary },
  template: `<TaChipPrimary
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
};
