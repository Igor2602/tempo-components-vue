import TaChipPrimaryOutlined from './TaChipPrimaryOutlined.vue';

export default {
  title: 'Design System/TaChip/Primary/TaChipPrimaryOutlined',
  component: TaChipPrimaryOutlined,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipPrimaryOutlined },
  template: `<TaChipPrimaryOutlined
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
};
