import TaChipSecondary from './TaChipSecondary.vue';

export default {
  title: 'Design System/TaChip/Secondary/TaChipSecondary',
  component: TaChipSecondary,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipSecondary },
  template: `<TaChipSecondary
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
};
