import TaChipSecondary from './TaChipSecondary.vue';

export default {
  title: 'Design System/TaChip/Secondary/TaChipSecondaryClose',
  component: TaChipSecondary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipSecondary },
  template: `<TaChipSecondary
    v-if="taChip"
    :label="label"
    close
    @click:close="taChip = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
  taChip: true,
};
