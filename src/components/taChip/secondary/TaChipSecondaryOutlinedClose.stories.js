import TaChipSecondaryOutlined from './TaChipSecondaryOutlined.vue';

export default {
  title: 'Design System/TaChip/Secondary/TaChipSecondaryOutlinedClose',
  component: TaChipSecondaryOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipSecondaryOutlined },
  template: `<TaChipSecondaryOutlined
    v-if="taChip"
    :label="label"
    close
    @click:close="taChip = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
  taChip: true,
};
