import TaChipSecondaryOutlined from './TaChipSecondaryOutlined.vue';

export default {
  title: 'Design System/TaChip/Secondary/TaChipSecondaryOutlined',
  component: TaChipSecondaryOutlined,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipSecondaryOutlined },
  template: `<TaChipSecondaryOutlined
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
};
