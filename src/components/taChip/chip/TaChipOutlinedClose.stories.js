import TaChipOutlined from './TaChipOutlined.vue';

export default {
  title: 'Design System/TaChip/Chip/TaChipOutlinedClose',
  component: TaChipOutlined,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipOutlined },
  template: `<TaChipOutlined
    v-if="taChip"
    :label="label"
    close
    @click:close="taChip = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
  taChip: true,
};
