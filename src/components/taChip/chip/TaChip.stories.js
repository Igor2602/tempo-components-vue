import TaChip from './TaChip.vue';

export default {
  title: 'Design System/TaChip/Chip/TaChip',
  component: TaChip,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChip },
  template: `<TaChip
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
};
