import TaChipOutlined from './TaChipOutlined.vue';

export default {
  title: 'Design System/TaChip/Chip/TaChipOutlined',
  component: TaChipOutlined,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaChipOutlined },
  template: `<TaChipOutlined
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Chip',
};
