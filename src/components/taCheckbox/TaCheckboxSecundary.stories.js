import TaCheckboxSecundary from './TaCheckboxSecundary.vue';

export default {
  title: 'Design System/TaCheckbox/TaCheckboxSecundary',
  component: TaCheckboxSecundary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onInput: { action: 'input' },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCheckboxSecundary },
  template: `<TaCheckboxSecundary
    v-model="primaryCheckbox"
    :label='label'
    @click="onClick"
    :input-value="primaryCheckbox"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'secundaryCheckbox',
  primaryCheckbox: false,
  disabled: false,
};
