import TaCheckboxPrimary from './TaCheckboxPrimary.vue';

export default {
  title: 'Design System/TaCheckbox/TaCheckboxPrimary',
  component: TaCheckboxPrimary,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onInput: { action: 'input' },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCheckboxPrimary },
  template: `<TaCheckboxPrimary
    v-model="primaryCheckbox"
    :label='label'
    @click="onClick"
    :input-value="primaryCheckbox"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Checkbox',
  primaryCheckbox: false,
  disabled: false,
};
