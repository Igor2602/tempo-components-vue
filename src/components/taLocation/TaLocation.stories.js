import TaLocation from './TaLocation.vue';

export default {
  title: 'Design System/TaLocation/TaLocation',
  component: TaLocation,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLocation },
  template: `<TaLocation
    @onInputValue="onClick"
    @select-item="onClick"
    :locationItems="locationItems"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  locationItems: [
    {
      street: 'Rua Emilio Pedutti',
      district: 'Morumbi',
      state: 'São Paulo',
      acronym: 'SP',
    },
    {
      street: 'Rua Emilio Pestana',
      district: 'Vila Claudia',
      state: 'São Paulo',
      acronym: 'SP',
    },
  ],
};
