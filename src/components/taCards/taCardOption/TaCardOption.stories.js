import TaCardOption from './TaCardOption.vue';

export default {
  title: 'Design System/TaCard/TaCardOption',
  component: TaCardOption,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCardOption },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
    <TaCardOption
      :items="itemsCardOption"
      @selectItem="onClick"
    />
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  itemsCardOption: [
    {
      icon: 'mdi-credit-card',
      label: 'Cartão de Credito',
    },
    {
      label: 'Pix',
      image: 'iconPix.svg',
    },
    {
      icon: 'mdi-help-circle',
      label: 'Opções',
    },
  ],
};
