import TaCardOptionGroup from './TaCardOptionGroup.vue';

export default {
  title: 'Design System/TaCard/TaCardOptionGroup',
  component: TaCardOptionGroup,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCardOptionGroup },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
  <TaCardOptionGroup
    :title="title"
    :items="taBtnGroup"
    v-model="toggle_multiple"
    @item-change="onClick"
  />
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  toggle_multiple: [],
  taBtnGroup: [
    {
      label: 'SIM',
      disabled: false,
      id: 0,
    },
    {
      label: 'NÃO',
      disabled: false,
      id: 1,
    },
  ],
  title: 'O veículo se encontra no subsolo?',
};
