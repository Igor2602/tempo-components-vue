import TaCard from './TaCard.vue';

export default {
  title: 'Design System/TaCard/TaCard',
  component: TaCard,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCard },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
  <v-row>
    <v-col md="6">
      <TaCard
        :code="code"
        :title="title"
        :type="type"
        :description="description"
        :action="action"
        outlined
        @action="onClick"
      />
    </v-col>

  <v-col md="6">
    <TaCard
      :code="code"
      :title="title"
      :type="type"
      :description="description"
      :action="action"
      :chip="chip"
      :colorChip="colorChip"
      @action="onClick"
    />
  </v-col>
  </v-row>

  <v-row>
    <v-col>
      <TaCard
        :code="code"
        :title="title"
        :type="type"
        :description="description"
        :action="action"
        :chip="chip"
        :colorChip="colorChip"
        @action="onClick"
      />
    </v-col>

    <v-col>
      <TaCard
        :code="code"
        :title="title"
        :type="type"
        :description="description"
        :action="action"
        :chip="chip"
        :colorChip="colorChip"
        @action="onClick"
      />
    </v-col>
  </v-row>
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  code: 'Serviço # 5562',
  title: 'Conserto de pia',
  type: 'Residencial',
  description: 'Sifão da pia da cozinha está com vazamento.',
  action: 'Detalhes',
  chip: 'Emergencial',
  colorChip: 'red',
};
