import TaCardDetailSecundary from './TaCardDetailSecundary.vue';

export default {
  title: 'Design System/TaCard/TaCardDetailSecundary',
  component: TaCardDetailSecundary,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCardDetailSecundary },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
    <TaCardDetailSecundary
      :avatarSize="avatarSize"
      :avatarTitle="avatarTitle"
      :avatarSubtitle="avatarSubtitle"
      :title="title"
      :code="code"
      :description="description"
      :labelBtn="labelBtn"
      :subtitle="subtitle"
      @click.native="onClick"
    />
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  avatarSize: 40,
  avatarTitle: 'Tatiana Shmayluk',
  avatarSubtitle: 'Cliente',
  title: 'Conserto de pia',
  subtitle: 'Residencia',
  code: '#05562',
  description: 'Sifão da pia da cozinha está com vazamento.',
  labelBtn: 'Detalhes',
};
