import TaCardDetailSeal from './TaCardDetailSeal.vue';

export default {
  title: 'Design System/TaCard/TaCardDetailSeal',
  component: TaCardDetailSeal,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCardDetailSeal },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
    <TaCardDetailSeal
      :title="title"
      :subtitle="subtitle"
      :code="code"
      :description="description"
      :labelBtn="labelBtn"
      :chip="chip"
      :colorChip="colorChip"
      @click.native="onClick"
    />
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  title: 'Conserto de pia',
  subtitle: 'Residencia',
  code: '#05562',
  description: 'Sifão da pia da cozinha está com vazamento.',
  labelBtn: 'Detalhes',
  chip: 'Emergencial',
  colorChip: 'red',
};
