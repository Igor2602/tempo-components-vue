import TaCardInfo from './TaCardInfo.vue';

export default {
  title: 'Design System/TaCard/TaCardInfo',
  component: TaCardInfo,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCardInfo },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
    <TaCardInfo
      :avatarType="'image'"
      :avatarImage="avatarImage"
      :avatarSize="avatarSize"
      :avatarTitle="avatarTitle"
      :avatarSubtitle="avatarSubtitle"
      :title="title"
      :code="code"
    />
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  avatarSize: 40,
  avatarTitle: 'Tatiana Shmayluk',
  avatarSubtitle: 'Cliente',
  title: 'Conserto de pia',
  code: '#05562',
  avatarImage: 'https://cdn.vuetifyjs.com/images/john.jpg',
};
