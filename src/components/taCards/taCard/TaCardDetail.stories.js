import TaCardDetail from './TaCardDetail.vue';

export default {
  title: 'Design System/TaCard/TaCardDetail',
  component: TaCardDetail,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCardDetail },
  template: `
  <div style="background: #D9D8D9; padding: 50px">
    <TaCardDetail
      :avatarSize="avatarSize"
      :avatarTitle="avatarTitle"
      :avatarSubtitle="avatarSubtitle"
      :title="title"
      :code="code"
      :labelBtn="labelBtn"
      @click.native="onClick"
    />
  </div>
  `,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  avatarSize: 40,
  avatarTitle: 'Tatiana Shmayluk',
  avatarSubtitle: 'Cliente',
  title: 'Conserto de pia',
  code: '#05562',
  labelBtn: 'Detalhes',
};
