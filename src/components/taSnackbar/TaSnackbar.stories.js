import TaSnackbar from './TaSnackbar.vue';

export default {
  title: 'Design System/TaSnackbar/TaSnackbar',
  component: TaSnackbar,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSnackbar },
  template: `<div>
    <TaSnackbar
      :model="model"
      :timeout="timeout"
      :bgColor="bgColor"
      :fontColor="fontColor"
      :text="text"
      :type="null"
      @close-snackbar="model = false"
    />

    <v-row class="mt-5">
      <v-col>
        <v-btn
          @click="model = true"
          color="#0096D3"
          class="color"
        >
          Abrir Snackbar
        </v-btn>
      </v-col>
    </v-row>
  </div>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  model: false,
  timeout: 5000,
  bgColor: '#0096D3',
  fontColor: '#ffffff',
  text: 'Teste de Notificação',
};
