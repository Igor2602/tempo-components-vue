import { Brand } from '@/utils/images.utils';
import TaDrawer from './TaDrawer.vue';

export default {
  title: 'Design System/TaDrawer/TaDrawer',
  component: TaDrawer,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaDrawer },
  template: `
  <div>
    <v-app app :style="{background: $vuetify.theme.themes[theme].background}">
      <TaDrawer
        :items="items"
        :brand="brand"
        :drawer="drawer"
        @on-navigate="onClick"
        @hide-drawer="drawer = false"
      />
    </v-app>
  </div>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  drawer: false,
  brand: Brand,
  theme: 'light',
  items: [
    {
      title: 'Home',
      router: '/',
      id: 0,
      icon: 'mdi-train-variant',
      enable: true,
      active: true,
    },
    {
      title: 'TaAccodion',
      router: '/accordion',
      id: 1,
      icon: 'mdi-train-variant',
      enable: true,
      active: false,
    },
    {
      title: 'TaAlers',
      router: '/alerts',
      id: 2,
      icon: 'mdi-train-variant',
      enable: true,
      active: false,
    },
    {
      title: 'TaAvalation',
      router: '/avaliation',
      id: 3,
      icon: 'mdi-train-variant',
      enable: true,
      active: false,
    },
    {
      title: 'TaAvatr',
      router: '/avatar',
      id: 4,
      icon: 'mdi-train-variant',
      enable: true,
      active: false,
    },
  ],
};
