import TaAlertSecondaryActionError from './TaAlertSecondaryActionError.vue';

export default {
  title: 'Design System/TaAlert/Action/TaAlertSecondaryActionError',
  component: TaAlertSecondaryActionError,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertSecondaryActionError },
  template: `<TaAlertSecondaryActionError
    :controlShow="controlShow"
    :label="label"
    :actionLabel="actionLabel"
    @action="onClick"
    @close="controlShow = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  actionLabel: 'ACTION',
  controlShow: true,
};
