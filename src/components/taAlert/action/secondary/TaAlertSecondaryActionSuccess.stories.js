import TaAlertSecondaryActionSuccess from './TaAlertSecondaryActionSuccess.vue';

export default {
  title: 'Design System/TaAlert/Action/TaAlertSecondaryActionSuccess',
  component: TaAlertSecondaryActionSuccess,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertSecondaryActionSuccess },
  template: `<TaAlertSecondaryActionSuccess
    :controlShow="controlShow"
    :label="label"
    :actionLabel="actionLabel"
    @action="onClick"
    @close="controlShow = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  actionLabel: 'ACTION',
  controlShow: true,
};
