import TaAlertPrimaryActionError from './TaAlertPrimaryActionError.vue';

export default {
  title: 'Design System/TaAlert/Action/TaAlertPrimaryActionError',
  component: TaAlertPrimaryActionError,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertPrimaryActionError },
  template: `<TaAlertPrimaryActionError
    :controlShow="controlShow"
    :label="label"
    :actionLabel="actionLabel"
    @action="onClick"
    @close="controlShow = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  actionLabel: 'ACTION',
  controlShow: true,
};
