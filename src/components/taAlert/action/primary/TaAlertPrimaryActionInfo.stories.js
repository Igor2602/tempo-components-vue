import TaAlertPrimaryActionInfo from './TaAlertPrimaryActionInfo.vue';

export default {
  title: 'Design System/TaAlert/Action/TaAlertPrimaryActionInfo',
  component: TaAlertPrimaryActionInfo,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertPrimaryActionInfo },
  template: `<TaAlertPrimaryActionInfo
    :controlShow="controlShow"
    :label="label"
    :actionLabel="actionLabel"
    @action="onClick"
    @close="controlShow = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  actionLabel: 'ACTION',
  controlShow: true,
};
