import TaAlertSecondarySuccess from './TaAlertSecondarySuccess.vue';

export default {
  title: 'Design System/TaAlert/Alert/TaAlertSecondarySuccess',
  component: TaAlertSecondarySuccess,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertSecondarySuccess },
  template: `<TaAlertSecondarySuccess
    :controlShow="controlShow"
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  controlShow: true,
};
