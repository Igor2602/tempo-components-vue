import TaAlertSecondaryInfo from './TaAlertSecondaryInfo.vue';

export default {
  title: 'Design System/TaAlert/Alert/TaAlertSecondaryInfo',
  component: TaAlertSecondaryInfo,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertSecondaryInfo },
  template: `<TaAlertSecondaryInfo
    :controlShow="controlShow"
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  controlShow: true,
};
