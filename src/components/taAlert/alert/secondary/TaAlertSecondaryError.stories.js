import TaAlertSecondaryError from './TaAlertSecondaryError.vue';

export default {
  title: 'Design System/TaAlert/Alert/TaAlertSecondaryError',
  component: TaAlertSecondaryError,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertSecondaryError },
  template: `<TaAlertSecondaryError
    :controlShow="controlShow"
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  controlShow: true,
};
