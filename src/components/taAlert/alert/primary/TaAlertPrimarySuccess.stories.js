import TaAlertPrimarySuccess from './TaAlertPrimarySuccess.vue';

export default {
  title: 'Design System/TaAlert/Alert/TaAlertPrimarySuccess',
  component: TaAlertPrimarySuccess,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertPrimarySuccess },
  template: `<TaAlertPrimarySuccess
    :controlShow="controlShow"
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  controlShow: true,
};
