import TaAlertPrimaryError from './TaAlertPrimaryError.vue';

export default {
  title: 'Design System/TaAlert/Alert/TaAlertPrimaryError',
  component: TaAlertPrimaryError,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertPrimaryError },
  template: `<TaAlertPrimaryError
    :controlShow="controlShow"
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  controlShow: true,
};
