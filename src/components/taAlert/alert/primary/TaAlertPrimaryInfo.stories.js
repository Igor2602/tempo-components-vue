import TaAlertPrimaryInfo from './TaAlertPrimaryInfo.vue';

export default {
  title: 'Design System/TaAlert/Alert/TaAlertPrimaryInfo',
  component: TaAlertPrimaryInfo,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaAlertPrimaryInfo },
  template: `<TaAlertPrimaryInfo
    :controlShow="controlShow"
    :label="label"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Texto de alerta',
  controlShow: true,
};
