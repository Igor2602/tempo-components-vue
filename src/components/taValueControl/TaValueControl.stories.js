import TaValueControl from './TaValueControl.vue';

export default {
  title: 'Design System/TaValueControl/TaValueControl',
  component: TaValueControl,
  argTypes: {
    onInput: { action: 'input' },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaValueControl },
  template: `<v-card class="pa-5 mb-5">
      <v-row>
        <v-col>
          <h3>TaValueControl</h3>
          <v-divider></v-divider>
        </v-col>
      </v-row>
      <v-row>
        <v-col>
          <p>Default</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Editable</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :editable="editable"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Percentage</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :percentage="percentage"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Limited</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :editable="editable"
            :limitValue="limitValue"
            :haveLimit="haveLimit"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>
      </v-row>

      <v-row>
        <v-col>
          <p>Currency</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :currency="currency"
            :editable="editable"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>
        <v-col>
          <p>Percentage and Editable</p>
          <TaValueControl
            :counter="counter"
            :editable="editable"
            :colorIcon="colorIcon"
            :percentage="percentage"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Percentage and Have limit</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :percentage="percentage"
            :limitValue="limitValue"
            :haveLimit="haveLimit"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Percentage and Editable and Have limit</p>
          <TaValueControl
            :counter="counter"
            :editable="editable"
            :colorIcon="colorIcon"
            :percentage="percentage"
            :limitValue="limitValue"
            :haveLimit="haveLimit"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>
      </v-row>

      <v-row>
        <v-col>
          <p>Currency no Editable</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :currency="currency"
            :editable="false"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Disabled</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :currency="currency"
            :disabled="disabled"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Percentage Decimal</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :percentage="percentage"
            :editable="editable"
            :decimal="decimal"
            :item="item"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>

        <v-col>
          <p>Currency Decimal</p>
          <TaValueControl
            :counter="counter"
            :colorIcon="colorIcon"
            :currency="currency"
            :editable="editable"
            :decimal="decimal"
            :item="item"
            @change-input-value="OnInput"
            @item="onClick"
          />
        </v-col>
      </v-row>
    </v-card>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  counter: 0,
  colorIcon: '#2E4055',
  editable: true,
  percentage: true,
  limitValue: 2,
  currency: true,
  disabled: true,
  decimal: true,
  item: {
    id: 996,
  },
  haveLimit: {
    have: true,
    value: 2,
  },
};
