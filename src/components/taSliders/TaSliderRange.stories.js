import TaSliderRange from './TaSliderRange.vue';

export default {
  title: 'Design System/TaSlider/TaSliderRange',
  component: TaSliderRange,
  argTypes: {
    input: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSliderRange },
  template: `<TaSliderRange
    v-model="sliderRange"
    @input="input($event)"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  sliderRange: [20, 40],
};
