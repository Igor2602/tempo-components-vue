import TaSlider from './TaSlider.vue';

export default {
  title: 'Design System/TaSlider/TaSliderIcon',
  component: TaSlider,
  argTypes: {
    input: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSlider },
  template: `<TaSlider
    v-model="primarySlider"
    :prepend-icon="icon"
    @input="input($event)"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  primarySlider: 25,
  icon: 'mdi-volume-high',
};
