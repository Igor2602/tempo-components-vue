import TaDialogConfirmation from './TaDialogConfirmation.vue';

export default {
  title: 'Design System/TaDialog/TaDialogConfirmation',
  component: TaDialogConfirmation,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaDialogConfirmation },
  template: `
  <TaDialogConfirmation
    v-model="dialog"
    :title="title"
    :description="description"
    :confirmLabel="confirmLabel"
    :cancelLabel="cancelLabel"
    :columnButton="columnButton"
    @primary-button="onClick"
    @secundary-button="dialog = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  dialog: true,
  title: 'Usar geolocalização',
  description: 'Você permite que esse site acesse a localização atual do seu dispositivo?',
  confirmLabel: 'Permitir',
  cancelLabel: 'Não permitir',
  columnButton: true,
};
