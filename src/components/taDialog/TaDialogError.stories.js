import TaDialogConfirmation from './TaDialogConfirmation.vue';

export default {
  title: 'Design System/TaDialog/TaDialogError',
  component: TaDialogConfirmation,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaDialogConfirmation },
  template: `
  <TaDialogConfirmation
    v-model="dialog"
    :title="title"
    :description="description"
    :confirmLabel="confirmLabel"
    :columnButton="columnButton"
    :closeOption="closeOption"
    :titleClass="titleClass"
    @primary-button="dialog = false"
    @close="dialog = false"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  dialog: true,
  title: 'Ops, algo deu errado',
  description: 'O arquivo que você tentou enviar é inválido.Ele precisa estar em uma das extensões abaixo:.xlsx, .xls ou .xlsm',
  confirmLabel: 'TENTAR NOVAMENTE',
  columnButton: true,
  closeOption: true,
  titleClass: 'title_error',
};
