import TaDialogList from './TaDialogList.vue';

export default {
  title: 'Design System/TaDialog/TaDialogList',
  component: TaDialogList,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaDialogList },
  template: `
  <TaDialogList
    v-model="dialogParcel"
    persistent
    width="310"
    :title="title"
    :items="dialogParcelItems"
    :dialogCurrency="dialogCurrency"
    @close-modal="dialogParcel = false"
    @select-item="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  title: 'Pagar em quantas vezes?',
  dialogCurrency: true,
  dialogParcel: true,
  dialogParcelItems: [
    {
      parcel: 1,
      price: 169.00,
    },
    {
      parcel: 2,
      price: 84.95,
    },
    {
      parcel: 3,
      price: 56.63,
    },
  ],
};
