import TaDialogSlot from './TaDialogSlot.vue';

export default {
  title: 'Design System/TaDialog/TaDialogSlot',
  component: TaDialogSlot,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaDialogSlot },
  template: `
    <TaDialogSlot
      v-model="dialogSlot"
      persistent
      width="310"
      :dialogHeader="dialogHeader"
      @close-modal="dialogSlot = false"
    >
      <template
        v-slot:content
      >
        <div>
          <h1>slot content</h1>
        </div>
      </template>
    </TaDialogSlot>`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  dialogSlot: true,
  dialogHeader: {
    dialogTitle: 'Dialog Title',
    closeIcon: 'mdi-close',
    iconColor: 'primary',
    titleColor: '#2E4055',
  },
};
