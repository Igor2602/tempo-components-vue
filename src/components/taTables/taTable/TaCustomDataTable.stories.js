import { v4 as uuidv4 } from 'uuid';
import TaCustomDataTable from './TaCustomDataTable.vue';

export default {
  title: 'Design System/TaTables/TaTable/TaCustomDataTable',
  component: TaCustomDataTable,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaCustomDataTable },
  template: `<TaCustomDataTable
    :headers="headers"
    :items="items"
    :loading="loadingTable"
    :buttonsContent="buttonsContent"
    :pagination="pagination"
    :rangePicker="rangePicker"
    @change-table="onClick"
    @download-consolidada="onClick"
    @download-master="onClick"
    @download-change="onClick"
    @button-range-change="onClick"
    @generate-master="onClick"
    @detail-change="onClick"
    @date-select="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  headers: [
    {
      text: 'Título 1',
      align: 'start',
      value: 'title1',
      type: null,
      key: 0,
    },
    {
      text: 'Título 2',
      align: 'start',
      value: 'phone',
      type: null,
      key: 1,
    },
    {
      text: 'Título 3',
      align: 'start',
      value: 'date',
      type: null,
      key: 2,
    },
    {
      text: 'Título 4',
      align: 'start',
      value: 'dateHour',
      type: null,
      key: 3,
    },
    {
      text: 'Título 5',
      align: 'start',
      value: 'zipCode',
      type: null,
      key: 4,
    },

    {
      text: 'Título 6',
      align: 'start',
      value: 'currency',
      type: null,
      key: 5,
    },

    {
      text: '',
      align: 'start',
      value: 'actions',
      type: 'download',
      key: 6,
      sortable: false,
    },
  ],
  items: [
    {
      title1: 'Texto 1',
      phone: '11999999998',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 34.00,
      actions: [
        {
          type: 'button-range',
          value: false,
          trigger: 'button-range-change',
          label: 'Definir Período',
          model: null,
          active: false,
          disabled: false,
        },
      ],
    },
    {
      title1: 'Texto 2',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: true,
        },
      ],
    },
    {
      title1: 'Texto 3',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: true,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 4',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'button-range',
          value: false,
          trigger: 'button-range-change',
          label: '01/02 - 25/02',
          model: null,
          active: true,
          disabled: false,
        },
      ],
    },
    {
      title1: 'Texto 5',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'button-range',
          value: false,
          trigger: 'button-range-change',
          label: '01/02 - 25/02',
          model: null,
          active: true,
          disabled: true,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'detail',
          trigger: 'detail-change',
        },
      ],
    },
    {
      title1: 'Texto 2',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'upload',
          value: false,
          trigger: 'upload-change',
          disabled: false,
          active: true,
        },
      ],
    },
    {
      title1: 'Texto 3',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'upload',
          value: false,
          trigger: 'upload-change',
          disabled: true,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
    {
      title1: 'Texto 6',
      phone: '11999999999',
      date: new Date(),
      id: uuidv4(),
      range: [],
      dateHour: new Date(),
      zipCode: '06340650',
      currency: 35.00,
      actions: [
        {
          type: 'download',
          value: false,
          trigger: 'download-change',
          disabled: false,
          active: false,
        },
      ],
    },
  ],
  pagination: {
    itemsPerPage: 10,
    page: 1,
    totalPages: 2,
    totalItems: 20,
    show: true,
    itensPerPage: 'Linhas por página',
  },
  loadingTable: false,
  buttonsContent: [
    {
      label: 'PLANILHA CONSOLIDADA',
      trigger: 'download-consolidada',
      disabled: false,
      type: 'TaBtnDownloadSecondarySmall',
    },
    {
      label: 'PLANILHA MASTER',
      trigger: 'download-master',
      type: 'TaBtnDownloadOutlinedSmall',
      disabled: false,
    },
    {
      label: 'GERAR PLANILHA MASTER',
      trigger: 'generate-master',
      type: 'TaBtnSecondarySmall',
      disabled: false,
    },
    {
      label: 'GERAR PLANILHA MASTER',
      trigger: 'generate-master',
      type: 'TaBtnOutlinedSmall',
      disabled: true,
    },
  ],
  rangePicker: true,
};
