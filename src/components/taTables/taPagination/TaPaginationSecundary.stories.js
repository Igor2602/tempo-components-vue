import TaPaginationSecundary from './TaPaginationSecundary.vue';

export default {
  title: 'Design System/TaTables/TaPagination/TaPaginationSecundary',
  component: TaPaginationSecundary,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaPaginationSecundary },
  template: `<TaPaginationSecundary
    v-model="paginationSecundary.page"
    :length="paginationSecundary.totalPages"
    :total-visible="paginationSecundary.visible"
    @input="onInput"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  paginationSecundary: {
    page: 2,
    totalPages: 16,
    visible: 10,
  },
};
