import TaPaginationPrimary from './TaPaginationPrimary.vue';

export default {
  title: 'Design System/TaTables/TaPagination/TaPaginationPrimary',
  component: TaPaginationPrimary,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaPaginationPrimary },
  template: `<TaPaginationPrimary
    v-model="paginationPrimary.page"
    :length="paginationPrimary.totalPages"
    :total-visible="paginationPrimary.visible"
    @input="onInput"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  paginationPrimary: {
    page: 1,
    totalPages: 16,
    visible: 10,
  },
};
