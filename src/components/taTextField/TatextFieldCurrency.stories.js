import TatextFieldCurrency from './TatextFieldCurrency.vue';

export default {
  title: 'Design System/TaTextField/TatextFieldCurrency',
  component: TatextFieldCurrency,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TatextFieldCurrency },
  template: `<TatextFieldCurrency
    v-model="currency"
    :label="label"
    :required="required"
    :input="onInput"
    :rules="rules.required"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Valor',
  currency: 0.00,
  required: true,
  rules: {
    required: [(v) => !!v || 'Esse campo é obrigatório'],
  },
};
