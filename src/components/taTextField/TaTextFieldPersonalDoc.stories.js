import IsPersonalDoc from '../../utils/validation-doc';
import TaTextFieldPersonalDoc from './TaTextFieldPersonalDoc.vue';

export default {
  title: 'Design System/TaTextField/TaTextFieldPersonalDoc',
  component: TaTextFieldPersonalDoc,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaTextFieldPersonalDoc },
  template: `<TaTextFieldPersonalDoc
    v-model="personalDoc"
    :label="label"
    :required="required"
    :input="onInput"
    :rules="rules.rulesCpf"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'CPF',
  personalDoc: null,
  required: true,
  rules: {
    rulesCpf: [
      (v) => !!v || 'Esse campo é obrigatório',
      (v) => IsPersonalDoc.personalDoc(v) || 'CPF inválido.',
    ],
  },
};
