import TaTextField from './TaTextField.vue';

export default {
  title: 'Design System/TaTextField/TaTextField',
  component: TaTextField,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaTextField },
  template: `<TaTextField
    v-model="name"
    :label="label"
    :required="required"
    :rules="rules.required"
    :input="onInput"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Nome Completo',
  name: null,
  required: true,
  rules: {
    required: [(v) => !!v || 'Esse campo é obrigatório'],
  },
};
