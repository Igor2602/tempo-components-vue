import TaTextFieldZipCode from './TaTextFieldZipCode.vue';

export default {
  title: 'Design System/TaTextField/TaTextFieldZipCode',
  component: TaTextFieldZipCode,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaTextFieldZipCode },
  template: `<TaTextFieldZipCode
    v-model="zipCode"
    :label="label"
    :required="required"
    :input="onInput"
    :rules="rules.required"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'CEP',
  zipCode: '99999-999',
  required: true,
  rules: {
    required: [(v) => !!v || 'Esse campo é obrigatório'],
  },
};
