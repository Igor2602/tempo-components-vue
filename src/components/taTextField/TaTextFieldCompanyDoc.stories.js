import IsCompanyDoc from '../../utils/validation-companyDoc';
import TaTextFieldCompanyDoc from './TaTextFieldCompanyDoc.vue';

export default {
  title: 'Design System/TaTextField/TaTextFieldCompanyDoc',
  component: TaTextFieldCompanyDoc,
  argTypes: {
    onInput: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaTextFieldCompanyDoc },
  template: `<TaTextFieldCompanyDoc
    v-model="companyDoc"
    :label="label"
    :required="required"
    :input="onInput"
    :rules="rules.cnpjRule"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'CNPJ',
  companyDoc: null,
  required: true,
  rules: {
    cnpjRule: [
      (v) => !!v || 'Esse campo é obrigatório',
      (v) => IsCompanyDoc.companyDoc(v) || 'CNPJ inválido.',
    ],
  },
};
