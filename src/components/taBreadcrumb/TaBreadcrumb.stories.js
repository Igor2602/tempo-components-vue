import TaBreadcrumb from './TaBreadcrumb.vue';

export default {
  title: 'Design System/TaBreadcrumb/TaBreadcrumb',
  component: TaBreadcrumb,
  argTypes: {
    input: { action: 'input' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaBreadcrumb },
  template: `
  <TaBreadcrumb
    :items="items"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  items: [
    {
      text: 'Compra de VT',
      disabled: false,
      href: '/teste',
    },
    {
      text: 'Anteriores',
      disabled: false,
      href: '/teste',
    },
  ],
};
