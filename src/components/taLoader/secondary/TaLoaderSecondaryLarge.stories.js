import TaLoaderSecondaryLarge from './TaLoaderSecondaryLarge.vue';

export default {
  title: 'Design System/TaLoader/TaLoaderSecondary/TaLoaderSecondaryLarge',
  component: TaLoaderSecondaryLarge,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLoaderSecondaryLarge },
  template: `<TaLoaderSecondaryLarge
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
