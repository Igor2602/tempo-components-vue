import TaLoaderSecondary from './TaLoaderSecondary.vue';

export default {
  title: 'Design System/TaLoader/TaLoaderSecondary/TaLoaderSecondary',
  component: TaLoaderSecondary,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLoaderSecondary },
  template: `<TaLoaderSecondary
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
