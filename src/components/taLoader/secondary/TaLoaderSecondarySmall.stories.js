import TaLoaderSecondarySmall from './TaLoaderSecondarySmall.vue';

export default {
  title: 'Design System/TaLoader/TaLoaderSecondary/TaLoaderSecondarySmall',
  component: TaLoaderSecondarySmall,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLoaderSecondarySmall },
  template: `<TaLoaderSecondarySmall
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
