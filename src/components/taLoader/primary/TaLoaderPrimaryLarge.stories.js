import TaLoaderPrimaryLarge from './TaLoaderPrimaryLarge.vue';

export default {
  title: 'Design System/TaLoader/TaLoaderPrimary/TaLoaderPrimaryLarge',
  component: TaLoaderPrimaryLarge,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLoaderPrimaryLarge },
  template: `<TaLoaderPrimaryLarge
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
