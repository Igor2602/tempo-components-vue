import TaLoaderPrimarySmall from './TaLoaderPrimarySmall.vue';

export default {
  title: 'Design System/TaLoader/TaLoaderPrimary/TaLoaderPrimarySmall',
  component: TaLoaderPrimarySmall,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLoaderPrimarySmall },
  template: `<TaLoaderPrimarySmall
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
