import TaLoaderPrimary from './TaLoaderPrimary.vue';

export default {
  title: 'Design System/TaLoader/TaLoaderPrimary/TaLoaderPrimary',
  component: TaLoaderPrimary,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaLoaderPrimary },
  template: `<TaLoaderPrimary
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
};
