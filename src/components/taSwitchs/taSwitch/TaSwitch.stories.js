import TaSwitch from './TaSwitch.vue';

export default {
  title: 'Design System/TaSwitch/TaSwitch',
  component: TaSwitch,
  argTypes: {
    disabled: {
      control: { type: 'select' },
      options: [true, false],
    },
    onInput: { action: 'input' },
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaSwitch },
  template: `<TaSwitch
    v-model="primarySwitch"
    :label="label"
    @click="onClick"
    :input-value="primarySwitch"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  label: 'Switch',
  primarySwitch: false,
  disabled: false,
};
