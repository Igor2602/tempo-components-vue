import TaRadioButton from './TaRadioButton.vue';

export default {
  title: 'Design System/TaRadioButton/TaRadioButton',
  component: TaRadioButton,
  argTypes: {
    onClick: { action: 'clicked' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TaRadioButton },
  template: `<TaRadioButton
    v-model="radioGroup"
    row
    :items="items"
    @change-radio="onClick"
  />`,
  methods: { toJSON: () => {} }, // bug vuetify
});

export const Default = Template.bind({});
Default.args = {
  radioGroup: 0,
  items: [
    {
      label: 'Radio 1',
      value: 0,
    },

    {
      label: 'Radio 2',
      value: 1,
    },
  ],
};
