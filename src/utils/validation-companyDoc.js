/* eslint-disable */
export default class IsCompanyDoc {
  static companyDoc = (v) => /(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)/.test(v);
}
