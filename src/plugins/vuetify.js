import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

const opts = {
  theme: {
    light: true,
    dark: false,
    themes: {
      light: {
        background: '#F2F2F3',
        black: '#1E1C1F',
        red: '#DB4F4F',
        primary: '#00527D',
        gray: '#5F5C60',
        green: '#35A97F',
        blue: '#0096D3',
        gray2: '#748C94',
        dark: '#1A1E2E',
        lightBlue: '#5EBED4',
        lightGray: '#D9D8D9',
        darkBlue: '#2A2F41',
        accent: '#82B1FF',
        error: '#FF5858',
        info: '#F2F2F3',
        success: '#35A97F',
        warning: '#FFC107',
        white: '#ffffff',
      },
      dark: {
        primary: '#0096D3',
        blue: '#5EBED4',
        lightGray: '#748C94',
        background: '#1A1E2E',
        black: '#1E1C1F',
        red: '#DB4F4F',
        gray: '#5F5C60',
        green: '#35A97F',
        gray2: '#5EBED4', // lightBlue
        dark: '#1A1E2E',
        lightBlue: '#5EBED4',
        darkBlue: '#2A2F41',
      },
    },
  },
};

export default new Vuetify(opts);
