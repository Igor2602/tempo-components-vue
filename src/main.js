import Vue from 'vue';
import VueTheMask from 'vue-the-mask';
import money from 'v-money';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;
Vue.use(VueTheMask);
Vue.use(money, { precision: 4 });

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
