# Builder
FROM node:14
ARG NODE_ENV=$NODE_ENV
ENV NODE_ENV=$NODE_ENV
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build
RUN ls -l dist/

# Application
FROM nginx:stable
RUN rm -f /usr/share/nginx/html/*
COPY --from=0 /app/dist/ /usr/share/nginx/html/
COPY conf.d/default.conf /etc/nginx/conf.d/default.conf